package com.taxi_time_user.app.sign;

public interface SignPresenter {
    void onButtonClicked(int index);
}
