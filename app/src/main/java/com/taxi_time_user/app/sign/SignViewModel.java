package com.taxi_time_user.app.sign;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.my_account.MyAccountViewModel;
import com.taxi_time_user.app.verification.VerificationActivity;
import com.taxi_time_user.app.models.Sign;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

public class SignViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator between view model and its view */
    private ObservableField<Boolean> signUpIn;/* flag to know if user is registering or logging in (true : register , false : login) */
    private ObservableField<Boolean> buttonsClickable;/* flag to disable or enable buttons */
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog */
    private String countryId;/* selected country id */

    public SignViewModel(@NonNull Application application) {
        super(application);
        signUpIn = new ObservableField<>(true);
        buttonsClickable = new ObservableField<>(true);
        progress = new ObservableField<>(View.GONE);
        generateFirebaseToken();
    }

    private void generateFirebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Log.e("LoginActivity", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.FIREBASE_TOKEN, token);
                    }
                });
    }

    public void requestSignUpIn(EditText usernameEditText,EditText emailEditText, String selectedCountryCode, EditText phoneEditText) {
        String username = usernameEditText.getText().toString();
        String phoneNumber = phoneEditText.getText().toString();
        String email = emailEditText.getText().toString();
        Sign sign;

        if (signUpIn.get().booleanValue()) {// register
            if (username.isEmpty()) {
                viewListener.showErrorMessage(usernameEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }
            if (email.isEmpty()) {
                viewListener.showErrorMessage(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }
            if (MyAccountViewModel.checkMailValidation(email)) {
                viewListener.showErrorMessage(phoneEditText, getApplication().getString(R.string.mail_not_valid));
                return;
            }
            if (phoneNumber.isEmpty()) {
                viewListener.showErrorMessage(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }

            if (countryId == null) {
                viewListener.showToastMessage(getApplication().getString(R.string.please_pick_your_country));
                return;
            }
            sign = new Sign(username,email, selectedCountryCode + "" + phoneNumber, "", countryId);
        } else {// login
            if (phoneNumber.isEmpty()) {
                viewListener.showErrorMessage(phoneEditText, getApplication().getString(R.string.this_field_can_not_be_blank));
                return;
            }

            sign = new Sign(selectedCountryCode + "" + phoneNumber, "");
        }


        String firebaseToken = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");
        if (firebaseToken.isEmpty())
            generateFirebaseToken();
        firebaseToken = MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.FIREBASE_TOKEN, "");

        sign.setFcmToken(firebaseToken);
        Intent intent = new Intent(getApplication(), VerificationActivity.class);
        intent.putExtra("signInfo", sign);
        intent.putExtra("signUpIn", signUpIn.get().booleanValue());
        viewListener.navigateTo(intent);
    }

    /*
        handle the action of each button in the activity
     */
    protected void handleButtonsAction(int index) {
        switch (index) {
            case 0:// register
                setSignUpIn(true);
                break;
            case 1:// login
                setSignUpIn(false);
                break;
            case 2: // sign button
                viewListener.startSignUpIn();
                break;
            case 4:// show countries dialog
                viewListener.displayCountriesDialog();
                break;
        }
    }


    // Setters & Getters --- start --- \\
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getSignUpIn() {
        return signUpIn;
    }

    public void setSignUpIn(boolean signUpIn) {
        this.signUpIn.set(signUpIn);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean buttonsClickable) {
        this.buttonsClickable.set(buttonsClickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }
    // Setters & Getters --- end --- \\

    protected interface ViewListener {
        void showToastMessage(String message);

        void displayCountriesDialog();

        void startSignUpIn();

        void showErrorMessage(EditText editText, String error);

        void navigateTo(Intent intent);
    }
}
