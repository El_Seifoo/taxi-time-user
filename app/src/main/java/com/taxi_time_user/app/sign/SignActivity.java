package com.taxi_time_user.app.sign;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import com.rilixtech.CountryCodePicker;
import com.taxi_time_user.app.BaseAuthActivity;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.country.CountriesDialogFragment;
import com.taxi_time_user.app.databinding.ActivitySignBinding;
import com.taxi_time_user.app.main.MainActivity;
import com.taxi_time_user.app.others.MySingleton;

public class SignActivity extends BaseAuthActivity implements SignPresenter, SignViewModel.ViewListener, CountriesDialogFragment.ParentListener {
    private ActivitySignBinding binding;
    private SignViewModel viewModel;
    private CountryCodePicker countryCodePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign);
        viewModel = new ViewModelProvider(this).get(SignViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        countryCodePicker = binding.countryCodePicker;
        countryCodePicker.registerPhoneNumberTextView(binding.phoneEditText);

        if (MySingleton.getInstance(this).isLoggedIn()) {
            Intent intent = new Intent(getApplication(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

    }

    @Override
    public void onButtonClicked(int index) {
        viewModel.handleButtonsAction(index);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void displayCountriesDialog() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            fragmentTransaction.remove(prev);
        }

        fragmentTransaction.addToBackStack(null);
        DialogFragment dialogFragment = new CountriesDialogFragment();
        dialogFragment.show(fragmentTransaction, "dialog");

    }

    @Override
    public void startSignUpIn() {
        viewModel.requestSignUpIn(binding.usernameEditText, binding.etEmail, countryCodePicker.getSelectedCountryCodeWithPlus(), binding.phoneEditText);
    }

    @Override
    public void showErrorMessage(EditText editText, String error) {
        editText.setError(error);
        editText.requestFocus();
    }

    @Override
    public void navigateTo(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void setCountryId(String countryName, String countryId) {
        binding.countryText.setText(countryName);
        viewModel.setCountryId(countryId);
    }
}
