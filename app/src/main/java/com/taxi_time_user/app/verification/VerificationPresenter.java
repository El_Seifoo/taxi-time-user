package com.taxi_time_user.app.verification;

public interface VerificationPresenter {
    void onVerifyButtonClicked();
}
