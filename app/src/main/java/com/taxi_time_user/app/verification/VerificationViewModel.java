package com.taxi_time_user.app.verification;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.taxi_time_user.app.main.MainActivity;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Sign;
import com.taxi_time_user.app.models.User;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerificationViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator betn. view model and its view*/
    private ObservableField<Integer> progress;/* flag to set state of loader (loading or not) */
    private ObservableField<Boolean> buttonsClickable; /* flag to set buttons clickable state (clickable or not) */

    public VerificationViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        call firebase method to check if code is correct or not
     */
    public void signInWithPhoneCredential(PhoneAuthCredential phoneAuthCredential, final Sign signInfo, final boolean signUpIn) {
        FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    if (isActive) {
                        Map<String, String> body = new HashMap<>();
                        if (signUpIn) {// register
                            body.put("name", signInfo.getUsername());
                            body.put("email", signInfo.getEmail());
                            body.put("country", signInfo.getCountryId());
                        }
                        body.put("phoneNumber", signInfo.getPhoneNumber());
                        body.put("fcm_token", signInfo.getFcmToken());
                        body.put("user_type", String.valueOf(signInfo.getUserType()));
                        signUpIn(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language)), body);
                    }
                    return;
                }

                if (task.getException() instanceof FirebaseAuthInvalidCredentialsException)
                    // The verification code entered was invalid
                    viewListener.showToastMessage(getApplication().getString(R.string.invalid_code));
            }
        });
    }

    /*
        handle error message (exception) of sending code to user phone
     */
    public void handleOnVerificationFailed(FirebaseException exception) {
        if (exception instanceof FirebaseAuthInvalidCredentialsException)
            viewListener.showToastMessage(getApplication().getString(R.string.invalid_phone_format));
        else
            viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
    }

    /*
        call firebase method to send otp code to user phone number
     */
    public void requestSendCode(PhoneAuthProvider.OnVerificationStateChangedCallbacks callback, String phoneNumber) {
        Log.e("verification", "req. send code");
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                15,
                TimeUnit.SECONDS,
                viewListener.returnActivity(),
                callback
        );
    }

    /*
        firebase usually make auto-verification ... send code to user and check the code without letting user write down the code
        to check if correct or not .. but sometime this doesn't happen so we use this method
        when user write down the code and click verify button ...
        @Param verificationId -> id sent from firebase
        @param otpCode -> code written down by user
     */
    public void handleVerificationButtonAction(String verificationId, String otpCode, Sign signInfo, boolean signUpIn) {
        // that means user does not need to write down his code
        if (verificationId == null) return;

        if (otpCode.length() < 6) {
            viewListener.showToastMessage(getApplication().getString(R.string.invalid_code));
            return;
        }

        signInWithPhoneCredential(PhoneAuthProvider.getCredential(verificationId, otpCode), signInfo, signUpIn);
    }

    private void signUpIn(String lang, final Map<String, String> body) {
        setButtonsClickable(false);
        setProgress(View.VISIBLE);

        Call<ApiResponse<String>> call = MySingleton.getInstance(getApplication()).createService(ApiServicesClient.class, 0).signUpIn(lang, body);

        call.enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                setButtonsClickable(true);
                setProgress(View.GONE);
                int code = response.code();
                if (code == 200) {
                    ApiResponse<String> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {
                        MySingleton.getInstance(getApplication()).saveUserData(new User(body.get("name"), body.get("phoneNumber")));
                        MySingleton.getInstance(getApplication()).saveStringToSharedPref(Constants.USER_TOKEN,
                                getApplication().getString(R.string.token_type).concat(" ").concat(apiResponse.getData()));
                        MySingleton.getInstance(getApplication()).loginUser();
                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        viewListener.navigateTo(intent);
                    } else {
                        viewListener.showToastMessage(apiResponse.getMessage());
                        // navigate back to let user edit his inputs
                        viewListener.navigateBack();
                    }

                } else if (code == 500) {
                    viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                    // navigate back to let user edit his inputs
                    viewListener.navigateBack();
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        viewListener.showToastMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                    } finally {
                        // navigate back to let user edit his inputs
                        viewListener.navigateBack();
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                setButtonsClickable(true);
                setProgress(View.GONE);
                viewListener.showToastMessage(t instanceof IOException ?
                        getApplication().getString(R.string.no_internet_connection) :
                        getApplication().getString(R.string.error_sending_data));
            }
        });
    }

    // Setters & Getters --- start --- \\
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }// Setters & Getters --- end --- \\


    private boolean isActive = true;

    @Override
    protected void onCleared() {
        super.onCleared();
        isActive = false;
    }


    protected interface ViewListener {
        void showToastMessage(String message);

        Activity returnActivity();

        void navigateTo(Intent intent);

        void navigateBack();
    }
}
