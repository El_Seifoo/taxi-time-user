package com.taxi_time_user.app.verification;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.FirebaseException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityVerificationBinding;
import com.taxi_time_user.app.models.Sign;

public class VerificationActivity extends AppCompatActivity implements VerificationViewModel.ViewListener, VerificationPresenter {
    private ActivityVerificationBinding binding;
    private VerificationViewModel viewModel;
    private String mVerificationId;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationStateChangedCallbacks;
    private Sign signInfo;
    private boolean signUpIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification);
        viewModel = new ViewModelProvider(this).get(VerificationViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        signInfo = (Sign) getIntent().getSerializableExtra("signInfo");
        signUpIn = getIntent().getExtras().getBoolean("signUpIn");

        verificationStateChangedCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                viewModel.signInWithPhoneCredential(phoneAuthCredential, signInfo, signUpIn);
            }

            @Override
            public void onVerificationFailed(@NonNull FirebaseException e) {
                viewModel.handleOnVerificationFailed(e);
            }

            @Override
            public void onCodeSent(@NonNull String verificationId, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(verificationId, forceResendingToken);
                mVerificationId = verificationId;
            }
        };

        viewModel.requestSendCode(verificationStateChangedCallbacks, signInfo.getPhoneNumber());
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public Activity returnActivity() {
        return this;
    }

    @Override
    public void navigateTo(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void navigateBack() {
        finish();
    }


    @Override
    public void onVerifyButtonClicked() {
        viewModel.handleVerificationButtonAction(mVerificationId, binding.otpView.getText().toString(), signInfo, signUpIn);
    }

    @Override
    public void onBackPressed() {
        if (viewModel.getButtonsClickable().get().booleanValue())
            super.onBackPressed();
    }
}
