package com.taxi_time_user.app.history;

public interface HistoryPresenter {
    void onDateClicked();

    void onRetry();
}
