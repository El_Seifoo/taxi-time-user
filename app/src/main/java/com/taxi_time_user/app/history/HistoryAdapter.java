package com.taxi_time_user.app.history;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.HistoryListItemBinding;
import com.taxi_time_user.app.models.Order;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.Holder> implements Filterable {
    private List<Order> orders;
    private List<Order> ordersFullList;
    private Context context;

    public HistoryAdapter(Context context) {
        this.context = context;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
        ordersFullList = new ArrayList<>(orders);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((HistoryListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.history_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.setOrder(orders.get(position));
    }

    @Override
    public int getItemCount() {
        return orders != null ? orders.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return orderFilter;
    }

    private Filter orderFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Order> filteredList = new ArrayList<>();

            if (constraint.equals(context.getString(R.string.all))) {
                filteredList.addAll(ordersFullList);
            } else {
                String filterPattern = constraint.toString();
                String dateString = "";
                for (Order order : ordersFullList) {
                    dateString = new SimpleDateFormat("dd-MM-yyyy").format(new Date(order.getRequestTime()));
                    if (dateString.equals(filterPattern)) {
                        filteredList.add(order);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            orders.clear();
            orders.addAll((List) results.values);
            if (orders.isEmpty()) {
                ((HistoryActivity) context).setEmptyListText(View.VISIBLE);
            } else {
                ((HistoryActivity) context).setEmptyListText(View.GONE);
            }
            notifyDataSetChanged();
        }
    };

    public class Holder extends RecyclerView.ViewHolder {
        private HistoryListItemBinding binding;

        public Holder(@NonNull HistoryListItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }
}
