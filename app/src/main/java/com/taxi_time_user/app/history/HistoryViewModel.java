package com.taxi_time_user.app.history;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Order;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryViewModel extends AndroidViewModel {
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<Integer> emptyListTextView;/* flag to know if the notifications list is empty or not */
    private ObservableField<String> errorMessage;/* error message of countries request */
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */
    private ObservableField<Boolean> buttonsClickable;/* flag to disable or enable buttons */

    public HistoryViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(true);
    }


    /*
        fetch all available notifications
     */
    private MutableLiveData<List<Order>> historyMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Order>> requestHistory() {
        if (historyMutableLiveData.getValue() == null) {
            loadHistory();
        }

        return historyMutableLiveData;
    }

    private void loadHistory() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        Call<ApiResponse<List<Order>>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .history(MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication()
                                        .getString(R.string.default_language)),
                        MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.USER_TOKEN, ""));

        call.enqueue(new Callback<ApiResponse<List<Order>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Order>>> call, Response<ApiResponse<List<Order>>> response) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<List<Order>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        List<Order> history = apiResponse.getData();
                        if (history == null) {
                            setEmptyListTextView(View.VISIBLE);// there are no history fetched
                            historyMutableLiveData.setValue(new ArrayList<Order>());
                        } else if (history.isEmpty()) {
                            setEmptyListTextView(View.VISIBLE);// there are no history fetched
                            historyMutableLiveData.setValue(history);
                        } else {// fetch history and set to the list
                            setEmptyListTextView(View.GONE);
                            historyMutableLiveData.setValue(history);
                        }
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Order>>> call, Throwable t) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                onFailureHandler(t);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        historyMutableLiveData.setValue(null);// when request fail make it equals null to be able to request history again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        historyMutableLiveData.setValue(null);// when request fail make it equals null to be able to request history again
        showResponseMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }


    // Setters & Getters --- start --- \\
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }
    // Setters & Getters --- end --- \\
}
