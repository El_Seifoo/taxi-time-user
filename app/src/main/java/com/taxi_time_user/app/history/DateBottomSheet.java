package com.taxi_time_user.app.history;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.DateBottomSheetLayoutBinding;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateBottomSheet extends BottomSheetDialogFragment implements BottomSheetPresenter {
    private DateBottomSheetLayoutBinding binding;
    private ParentCommunicator parentListener;
    private String[] days, months, years;

    public static DateBottomSheet newInstance() {
        return new DateBottomSheet();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.date_bottom_sheet_layout, container, false);

        binding.setPresenter(this);
        binding.setDaysPicker(binding.datePickerDays);
        binding.setMonthsPicker(binding.datePickerMonths);
        binding.setYearsPicker(binding.datePickerYears);

        final Calendar calendar = Calendar.getInstance();

        DateFormat dayFormat = new SimpleDateFormat("dd");
        days = new String[31];
        binding.setDayValue(Integer.parseInt(dayFormat.format(calendar.getTime())));
        for (int i = 0; i < 31; i++) {
            days[i] = (i + 1) + "";
        }

        binding.setDayMinValue(1);
        binding.setDayMaxValue(days.length);
        binding.setDays(days);

        months = getResources().getStringArray(R.array.months);
        DateFormat monthFormat = new SimpleDateFormat("MM");
        binding.setMonthValue(Integer.parseInt(monthFormat.format(calendar.getTime())));
        binding.setMonthMinValue(1);
        binding.setMonthMaxValue(months.length);
        binding.setMonths(months);


        years = new String[10];
        DateFormat yearFormat = new SimpleDateFormat("yyyy");
        years[0] = yearFormat.format(calendar.getTime());
        binding.setYearValue(1);
        for (int i = 1; i < 10; i++) {
            calendar.add(Calendar.YEAR, -1);
            years[i] = yearFormat.format(calendar.getTime());
        }


        binding.setYearMinValue(1);
        binding.setYearMaxValue(years.length);
        binding.setYears(years);

        parentListener = (ParentCommunicator) getContext();

        return binding.getRoot();
    }

    @Override
    public void onSaveClicked() {
        int month = binding.datePickerMonths.getValue();
        String date = days[binding.datePickerDays.getValue() - 1] + "-"
                + (month < 10 ? "0" + month : month) + "-"
                + years[binding.datePickerYears.getValue() - 1];
        parentListener.onSaveClicked(date);
        dismiss();
    }

    @Override
    public void onAllClicked() {
        parentListener.onSaveClicked(getString(R.string.all));
        dismiss();
    }

    @Override
    public void onCancelClicked() {
        dismiss();
    }

    public interface ParentCommunicator {
        void onSaveClicked(String date);
    }
}
