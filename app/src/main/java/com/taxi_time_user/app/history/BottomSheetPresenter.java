package com.taxi_time_user.app.history;

public interface BottomSheetPresenter {
    void onSaveClicked();

    void onAllClicked();

    void onCancelClicked();
}
