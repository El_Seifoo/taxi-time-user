package com.taxi_time_user.app.history;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityHistoryBinding;
import com.taxi_time_user.app.models.Order;

import java.util.List;

public class HistoryActivity extends AppCompatActivity implements HistoryPresenter, DateBottomSheet.ParentCommunicator {
    private ActivityHistoryBinding binding;
    private HistoryViewModel viewModel;
    private RecyclerView historyList;
    private HistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_history);
        viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);
        binding.setDate(getString(R.string.all));

        historyList = binding.historyList;
        historyList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapter = new HistoryAdapter(this);
        historyList.setAdapter(adapter);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        actionBar.setElevation(0);

        viewModel.requestHistory().observe(this, new Observer<List<Order>>() {
            @Override
            public void onChanged(List<Order> orders) {
                adapter.setOrders(orders);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDateClicked() {
        DateBottomSheet dateBottomSheet = DateBottomSheet.newInstance();
        dateBottomSheet.show(getSupportFragmentManager(), "date");
    }

    @Override
    public void onRetry() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestHistory();
    }

    protected void setEmptyListText(int empty) {
        viewModel.setEmptyListTextView(empty);
    }

    @Override
    public void onSaveClicked(String date) {
        binding.setDate(date);
        adapter.getFilter().filter(date);
    }
}
