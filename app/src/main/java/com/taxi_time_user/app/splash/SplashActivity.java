package com.taxi_time_user.app.splash;

import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.jaeger.library.StatusBarUtil;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivitySplashBinding;
import com.taxi_time_user.app.others.ActivityHelper;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

public class SplashActivity extends AppCompatActivity {

    private final static int SPLASH_DISPLAY_DELAY = 3000;
    private ActivitySplashBinding binding;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setTransparent(SplashActivity.this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

    }

    @Override
    protected void onStart() {
        super.onStart();
        handler = new Handler();
        handler.postDelayed(this::validation
                , SPLASH_DISPLAY_DELAY);
    }

    private void validation() {
        if (MySingleton.getInstance(this).isLoggedIn()) {
            ActivityHelper.setLocale(this,
                    MySingleton.getInstance(this)
                    .getStringFromSharedPref(Constants.APP_LANGUAGE,
                            getString(R.string.default_language)));
            ActivityHelper.startMainActivity(this, true);

        } else
            ActivityHelper.startLanguageActivity(this, true);

    }

}
