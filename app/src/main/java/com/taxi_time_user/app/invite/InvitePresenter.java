package com.taxi_time_user.app.invite;

public interface InvitePresenter {
    void onButtonClicked(int index);
}
