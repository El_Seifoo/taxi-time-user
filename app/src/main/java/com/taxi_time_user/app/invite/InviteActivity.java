package com.taxi_time_user.app.invite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityInviteBinding;
import com.taxi_time_user.app.others.MySingleton;

public class InviteActivity extends AppCompatActivity implements InvitePresenter {
    private ActivityInviteBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_invite);
        binding.setPresenter(this);
        binding.setInvitationCode(MySingleton.getInstance(this).userData().getPromoCode());
    }

    @Override
    public void onButtonClicked(int index) {
        switch (index) {
            case 0:
                finish();
                break;
            case 1:
                try {
                    Intent share = new Intent(Intent.ACTION_SEND)
                            .setType("text/plain")
                            .putExtra(Intent.EXTRA_TEXT, binding.getInvitationCode());
                    startActivity(share);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.sorry_unable_to_share), Toast.LENGTH_LONG).show();
                }
        }
    }
}
