package com.taxi_time_user.app.main.map;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.CarsTypesListItemBinding;
import com.taxi_time_user.app.models.Car;

import java.util.List;

public class CarsTypesAdapter extends RecyclerView.Adapter<CarsTypesAdapter.Holder> {
    private List<Car> cars;
    private final onCarsItemClicked listener;

    public CarsTypesAdapter(onCarsItemClicked listener) {
        this.listener = listener;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
        notifyDataSetChanged();
    }

    protected interface onCarsItemClicked {
        void onCarClickListener(Car car);
    }

    @NonNull
    @Override
    public CarsTypesAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CarsTypesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.cars_types_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CarsTypesAdapter.Holder holder, int position) {
        holder.binding.setCar(cars.get(position));
    }

    @Override
    public int getItemCount() {
        return cars != null ? cars.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CarsTypesListItemBinding binding;

        public Holder(@NonNull CarsTypesListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onCarClickListener(binding.getCar());
        }
    }
}
