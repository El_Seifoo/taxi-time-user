package com.taxi_time_user.app.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityMainBinding;
import com.taxi_time_user.app.databinding.NavHeaderBinding;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.taxi_time_user.app.sign.SignActivity;

public class MainActivity extends AppCompatActivity implements MainViewModel.ViewListener {
    private ActivityMainBinding binding;
    NavHeaderBinding navHeaderBinding;
    private MainViewModel viewModel;

    private NavigationView navigationView;
    private View navHeader;
    private DrawerLayout drawer;

    // tag for attached fragments
    protected static final String TAG_HOME = "home";//0

    // index to know the selected nav nav_menu item
    public int naveItemIndex = 0;

    // tag for the current attached fragment
    protected static String CURRENT_TAG = TAG_HOME;

    // flag to make decision when user press back button
    private boolean shouldLoadHomeFragOnBackPress = false;

    private Handler mHandler;
    private Runnable runnable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.setViewListener(this);

        mHandler = new Handler();

        drawer = binding.drawerLayout;
        navigationView = binding.navView;
        navigationView.setItemIconTintList(null);
        navHeader = navigationView.getHeaderView(0);


        // load navigation drawer header view
        navHeaderBinding = DataBindingUtil.bind(navHeader);
        loadHeader();


        // initialize navigation drawer
        setupNavigation();

        viewModel.checkSavedInstanceState(savedInstanceState);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadHeader();
    }

    private void loadHeader(){
        if (navHeaderBinding != null) {
            navHeaderBinding.setUser(MySingleton.getInstance(getApplicationContext()).userData());
        }
    }

    private void setupNavigation() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                return viewModel.handleOnNavigationItemSelected(menuItem);
            }
        });
    }

    @Override
    public void loadHomeFragment() {
        // close navigation drawer if user selected the current navigation item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            closeDrawer();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = viewModel.getSelectedFragment(naveItemIndex);
                if (fragment != null) {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.fade_in,
                            android.R.anim.fade_out);
                    transaction.replace(R.id.frame, fragment, CURRENT_TAG);
                    transaction.commitAllowingStateLoss();
                }
            }
        };

        if (runnable != null)
            mHandler.post(runnable);

        // close navigation drawer
        closeDrawer();

    }

    @Override
    public boolean navigateTo(Class destination) {
        startActivity(new Intent(this, destination));
        closeDrawer();
        return true;
    }

    @Override
    public boolean handleLogoutAction(Class destination) {
        MySingleton mySingleton = MySingleton.getInstance(getApplicationContext());

        String lang = mySingleton.getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language));
        mySingleton.logout();
        mySingleton.saveStringToSharedPref(Constants.APP_LANGUAGE, lang);

        finish();
        Intent intent = new Intent(this, SignActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        closeDrawer();
        return true;
    }

    @Override
    public void closeDrawer() {
        drawer.closeDrawers();
    }

    @Override
    public void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress) {
        this.naveItemIndex = naveItemIndex;
        CURRENT_TAG = currentTag;
        this.shouldLoadHomeFragOnBackPress = shouldLoadHomeFragOnBackPress;
    }

    @Override
    public void onBackPressed() {
        if (!viewModel.onBackBtnPressed(drawer.isDrawerOpen(GravityCompat.START), shouldLoadHomeFragOnBackPress))
            super.onBackPressed();
    }
}
