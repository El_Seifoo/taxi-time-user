package com.taxi_time_user.app.main.map;

import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.maps.android.PolyUtil;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Car;
import com.taxi_time_user.app.models.PlaceObj;
import com.taxi_time_user.app.models.PlacesResponse;
import com.taxi_time_user.app.models.RouteObj;
import com.taxi_time_user.app.models.direction_models.Directions;
import com.taxi_time_user.app.models.direction_models.Leg;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.GoogleMapsServicesClient;
import com.taxi_time_user.app.others.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator between viewModel and view */
    private ObservableField<Integer> emptyPlacesListTextView/* flag to know if places list is empty or not */,
            bottomSheetState/* flag to know the state of - pickup,destination - bottom sheet (expanded,collapsed,hidden) */,
            routingState/* flag to know if the route is drawn or not*/,
            routingErrorView/* flag to handle the visibility of drawing route error view */,
            carsTypesState/* flag to know state of cars types view (visible or not) */,
            carsTypesProgress/* flag to handle the visibility of cars types loader */,
            carsTypesErrorView/* flag to handle the visibility of cars types error view */,
            emptyCarsTypesListTextView/* flag to know if cars types list is empty or not */;
    private ObservableField<String> routingErrorMessage/* flag to handle the error message of drawing route req. */,
            carsTypesErrorMessage/* flag to handle the error message of fetching cars types req. */;
    private List<Address> pickUpAddress, destinationAddress;/* trip addresses (locations) */
    private ObservableField<Boolean> buttonsClickable/* flag to know if buttons are clickable or not */;
    private ObservableField<Double> distance/* distance between pickup and destination location */;

    public MapViewModel(@NonNull Application application) {
        super(application);
        emptyPlacesListTextView = new ObservableField<>(View.VISIBLE);
        bottomSheetState = new ObservableField<>(BottomSheetBehavior.STATE_COLLAPSED);
        routingState = new ObservableField<>(View.GONE);
        routingErrorView = new ObservableField<>(View.GONE);
        routingErrorMessage = new ObservableField<>("");
        pickUpAddress = new ArrayList<>();
        destinationAddress = new ArrayList<>();
        carsTypesState = new ObservableField<>(View.GONE);
        emptyCarsTypesListTextView = new ObservableField<>(View.GONE);
        carsTypesProgress = new ObservableField<>(View.GONE);
        carsTypesErrorView = new ObservableField<>(View.GONE);
        carsTypesErrorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(true);
        distance = new ObservableField<>(-1d);
    }

    /*
        call model fn to search for places
     */
    private MutableLiveData<List<PlaceObj>> placesMutableLiveData = new MutableLiveData<>();

    public MutableLiveData<List<PlaceObj>> requestPlaces(String query) {
        // call places api ..
        Call<PlacesResponse> call = MySingleton.getInstance(getApplication()).createService(GoogleMapsServicesClient.class, 1)
                .fetchPlaces(query,
                        getApplication().getString(R.string.google_maps_places_key),
                        MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language)));

        call.enqueue(new Callback<PlacesResponse>() {
            @Override
            public void onResponse(Call<PlacesResponse> call, Response<PlacesResponse> response) {
                PlacesResponse placesResponse = response.body();
                if (placesResponse.getStatus().equals(getApplication().getString(R.string.places_api_status_ok))) {// req. succeeded and list of places is fetched
                    setEmptyPlacesListTextView(View.GONE);
                    placesMutableLiveData.setValue(placesResponse.getPlaces());
                } else if (placesResponse.getStatus().equals(getApplication().getString(R.string.places_api_status_empty))
                        || placesResponse.getStatus().equals(getApplication().getString(R.string.places_api_status_invalid))) {// req. succeeded but list of places not fetched (empty list or query is null)
                    setEmptyPlacesListTextView(View.VISIBLE);
//                    placesMutableLiveData.setValue(new ArrayList<PlaceObj>());
                } else {
                    setEmptyPlacesListTextView(View.VISIBLE);
                    viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
//                    placesMutableLiveData.setValue(new ArrayList<PlaceObj>());
                }
            }

            @Override
            public void onFailure(Call<PlacesResponse> call, Throwable t) {
                onFailureHandler(t, 0);
            }
        });
        return placesMutableLiveData;
    }

    /* flag to know if user picked trip locations - pickup,destination- or not
       -1 -> no location is picked
        0 -> only pickup location is picked
        1 -> only destination location is picked
        2 -> both locations are picked
     */
    private int tripLocationsState = -1;

    /*
        handle what is going to happen when user pick location from places list
        extract list of addresses from Place object
        then update ui depending on tripLocationState and which location is picked
     */
    protected void handleOnPlaceItemAction(PlaceObj placeObj, EditText pickupEditText, EditText destinationEditText) {
        boolean pickUpFocused = pickupEditText.isFocused();
        Geocoder geocoder = new Geocoder(getApplication(), Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocationName(placeObj.getPlaceName().getMainName(), 1);
        } catch (IOException e) {
            e.printStackTrace();
            viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
        }

        if (addresses != null && addresses.size() > 0) {
            if (pickUpFocused) {// user is picking pickup location
                if (tripLocationsState == -1) {// no location was picked
                    tripLocationsState = 0;// notify that pickup location is picked
                    setTripLocation(addresses, pickupEditText, true, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 0) {// only pickup location was picked
                    setTripLocation(addresses, pickupEditText, true, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 1) {// only destination location was picked
                    tripLocationsState = 2;// notify that both location are picked
                    setTripLocation(addresses, pickupEditText, true, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 2) {// both locations are picked and user is editing location
                    setTripLocation(addresses, pickupEditText, true, placeObj.getPlaceName().getMainName());
                }
            } else {// user is picking destination location
                if (tripLocationsState == -1) {// no location was picked
                    tripLocationsState = 1;// notify that destination location is picked
                    setTripLocation(addresses, destinationEditText, false, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 0) {// only pickup location is picked
                    tripLocationsState = 2;// notify that both locations are picked
                    setTripLocation(addresses, destinationEditText, false, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 1) {// only destination location is picked
                    setTripLocation(addresses, destinationEditText, false, placeObj.getPlaceName().getMainName());
                } else if (tripLocationsState == 2) {// both locations are picked and user is editing location
                    setTripLocation(addresses, destinationEditText, false, placeObj.getPlaceName().getMainName());
                }

            }
        }

    }

    /*
        add address name to pickup/destination address editText
        set addresses to pickup/destination addresses
        start drawing routes depending on tripLocationState
     */
    private void setTripLocation(List<Address> addresses, EditText editText, boolean isPickup, String addressName) {
        if (isPickup) {
            pickUpAddress.clear();
            pickUpAddress.addAll(addresses);
        } else {
            destinationAddress.clear();
            destinationAddress.addAll(addresses);
        }
        viewListener.setLocationAddress(editText, addressName);

        if (tripLocationsState == 2) {
            startDrawingRoute(new LatLng(pickUpAddress.get(0).getLatitude(), pickUpAddress.get(0).getLongitude()),
                    new LatLng(destinationAddress.get(0).getLatitude(), destinationAddress.get(0).getLongitude()));
        }
    }

    /*
        user picked trip locations, now it's time to draw find and draw the best route
        between both locations
     */
    private void startDrawingRoute(LatLng pickup, LatLng destination) {
        // display route view ( route , back arrow)
        setRoutingState(View.VISIBLE);

        Map<String, String> map = new HashMap<>();
        map.put("origin", pickup.latitude + "," + pickup.longitude);
        map.put("destination", destination.latitude + "," + destination.longitude);
        map.put("language", "en");
        map.put("units", "metric");
        map.put("key", getApplication().getString(R.string.google_maps_places_key));

        // start drawing route
        Call<Directions> call = MySingleton.getInstance(getApplication()).createService(GoogleMapsServicesClient.class, 1).drawRoute(map);
        call.enqueue(new Callback<Directions>() {
            @Override
            public void onResponse(Call<Directions> call, Response<Directions> response) {// req. succeeded
                Directions directions = response.body();
                if (directions.getStatus().equals(getApplication().getString(R.string.places_api_status_ok))) {
                    Leg leg = directions.getRoutes().get(0).getLegs().get(0);
                    RouteObj route = new RouteObj(leg.getStartAddress(), leg.getEndAddress(), leg.getStartLocation().getLat(),
                            leg.getStartLocation().getLng(), leg.getEndLocation().getLat(), leg.getEndLocation().getLng(),
                            directions.getRoutes().get(0).getOverviewPolyline().getPoints());

                    // draw route
                    PolylineOptions polylineOptions = new PolylineOptions();
                    List<LatLng> pointsList = PolyUtil.decode(route.getOverviewPolyline());
                    for (LatLng latLng : pointsList) {
                        polylineOptions.add(latLng);
                    }

                    // set distance in Km (convert from meter to km)
                    distance.set(leg.getDistance().getValue() / 1000.5);

                    viewListener.drawRoute(polylineOptions,
                            new LatLng(route.getStartLat(), route.getStartLng()),
                            new LatLng(route.getEndLat(), route.getEndLng()));

                    // hide main bottom sheet
                    setBottomSheetState(BottomSheetBehavior.STATE_HIDDEN);

                    // start fetching cars types
                    viewListener.startFetchingCarsTypes();

                } else {// req. failed for someReason
                    setEmptyPlacesListTextView(View.VISIBLE);
                    viewListener.showToastMessage(getApplication().getString(R.string.something_went_wrong));
                    // hide route view ( route , back arrow)
                    setRoutingState(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Directions> call, Throwable t) {
                onFailureHandler(t, 1);
            }
        });

    }

    /*
        fetch cars types
     */
    private MutableLiveData<List<Car>> carsTypesMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Car>> requestCarsTypes() {
        setCarsTypesState(View.VISIBLE);
        setCarsTypesProgress(View.VISIBLE);
        setButtonsClickable(false);

        Call<ApiResponse<List<Car>>> call = MySingleton.getInstance(getApplication()).createService(ApiServicesClient.class, 0).fetchCarsTypes(
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key)),
                MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, "")
        );
        call.enqueue(new Callback<ApiResponse<List<Car>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Car>>> call, Response<ApiResponse<List<Car>>> response) {
                setCarsTypesProgress(View.GONE);
                setButtonsClickable(true);
                int code = response.code();
                if (code == 200) {
                    ApiResponse<List<Car>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        List<Car> cars = apiResponse.getData();
                        if (cars == null) {
                            setEmptyCarsTypesListTextView(View.VISIBLE);
                        } else if (cars.isEmpty()) {
                            setEmptyCarsTypesListTextView(View.VISIBLE);
                        } else {
                            setEmptyCarsTypesListTextView(View.GONE);
                            carsTypesMutableLiveData.setValue(cars);
                        }
                    }
                } else if (code == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data), 1);
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"), 1);
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data), 1);
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Car>>> call, Throwable t) {
                setCarsTypesProgress(View.GONE);
                setButtonsClickable(true);
                onFailureHandler(t, 2);
            }
        });
        return carsTypesMutableLiveData;
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message, int index) {
        if (index == 0) {
            setRoutingErrorView(View.VISIBLE);
            setRoutingErrorMessage(message);
        } else if (index == 1) {
            setCarsTypesErrorView(View.VISIBLE);
            setCarsTypesErrorMessage(message);
        }

    }

    /*
        handle error response of the request
        @param index ... flag to know which request is gonna be handled
                |-> 0: search for places
                |-> 1: draw routes
                |-> 2: cars types
     */
    public void onFailureHandler(Throwable t, int index) {
        switch (index) {
            case 0:
                viewListener.showToastMessage(getErrorMessage(t));
                break;
            case 1:
                // draw route error
                showResponseMessage(getErrorMessage(t), 0);
                break;
            case 2:
                // cars types error
                showResponseMessage(getErrorMessage(t), 1);
                break;
        }
    }


    private String getErrorMessage(Throwable t) {
        return t instanceof IOException
                ? getApplication().getString(R.string.no_internet_connection)
                : getApplication().getString(R.string.error_fetching_data);
    }

    protected void requestOrder(String carTypId) {
        /*
                {
                    "fromLat" : 30.7771607,
                    "fromLong" : 30.9769837,
                    "toLat" : 30.7790638,
                    "toLong" : 30.9897159,
                    "bookingType" : "5e453c793408df7b79110fc4"
                }

                                startDrawingRoute(new LatLng(pickUpAddress.get(0).getLatitude(), pickUpAddress.get(0).getLongitude()),
                        new LatLng(destinationAddress.get(0).getLatitude(), destinationAddress.get(0).getLongitude()));

         */

        Map<String, String> body = new HashMap<>();
        body.put("fromLat", String.valueOf(pickUpAddress.get(0).getLatitude()));
        body.put("fromLong", String.valueOf(pickUpAddress.get(0).getLongitude()));
        body.put("toLat", String.valueOf(destinationAddress.get(0).getLatitude()));
        body.put("toLong", String.valueOf(destinationAddress.get(0).getLongitude()));
        body.put("bookingType", carTypId);
    }

    /*
        handling back arrow icon action
        hide the visible items depending on param state
        and display the main bottom sheet (pickup, destination)
        TODO() continue the rest cases ..!!
     */
    protected void handleOnBackIconAction(int index) {
        switch (index) {
            case 1:// routing view is visible
                setRoutingState(View.GONE);

                // hide the view of cars types list
                setCarsTypesState(View.GONE);

                //remove polyLines
                viewListener.removeRoute();

                // clear destination address
                destinationAddress.clear();
//                tempDestinationAddresses = new ObservableField<>();
                tripLocationsState = 0;

                // clear distance and duration
//                setDuration(-1);
                distance.set(-1d);

                // if route was not drawn and error view is visible ... hide it
                if (routingErrorView.get() == View.VISIBLE) {
                    setRoutingErrorView(View.GONE);
                }
                break;
        }

        // show map location bottom sheet ( pickup , destination )
//        setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED);
        setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED);
    }

    /*
        retry any request if request failed at first time
        @Param index .. flag to know which request is gonna be requested again
                |-> 0 : drawing route betn. pickup & destination location
                |-> 1 : fetching cars types
                //TODO continue the rest cases
     */
    public void handleOnRetryAction(int index) {
        switch (index) {
            case 0:
                setRoutingErrorView(View.GONE);
                startDrawingRoute(new LatLng(pickUpAddress.get(0).getLatitude(), pickUpAddress.get(0).getLongitude()),
                        new LatLng(destinationAddress.get(0).getLatitude(), destinationAddress.get(0).getLongitude()));
                break;
            case 1:
                setCarsTypesErrorView(View.GONE);
                requestCarsTypes();
                break;
        }
    }


    // Setters & Getters --- start --- \\
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getEmptyPlacesListTextView() {
        return emptyPlacesListTextView;
    }

    public void setEmptyPlacesListTextView(int emptyPlacesListTextView) {
        if (emptyPlacesListTextView == View.VISIBLE)
            viewListener.clearPlacesAdapter();
        this.emptyPlacesListTextView.set(emptyPlacesListTextView);
    }

    public ObservableField<Integer> getBottomSheetState() {
        return bottomSheetState;
    }

    public void setBottomSheetState(int state) {
        this.bottomSheetState.set(state);
    }

    public ObservableField<Integer> getRoutingErrorView() {
        return routingErrorView;
    }

    public void setRoutingErrorView(int routingErrorView) {
        this.routingErrorView.set(routingErrorView);
    }

    public ObservableField<String> getRoutingErrorMessage() {
        return routingErrorMessage;
    }

    public void setRoutingErrorMessage(String routingErrorMessage) {
        this.routingErrorMessage.set(routingErrorMessage);
    }

    public ObservableField<Integer> getRoutingState() {
        return routingState;
    }

    public void setRoutingState(int visibility) {
        this.routingState.set(visibility);
    }

    public ObservableField<Integer> getCarsTypesProgress() {
        return carsTypesProgress;
    }

    public void setCarsTypesProgress(int carsTypesProgress) {
        this.carsTypesProgress.set(carsTypesProgress);
    }

    public ObservableField<Integer> getEmptyCarsTypesListTextView() {
        return emptyCarsTypesListTextView;
    }

    public void setEmptyCarsTypesListTextView(int visibility) {
        this.emptyCarsTypesListTextView.set(visibility);
    }

    public ObservableField<Integer> getCarsTypesState() {
        return carsTypesState;
    }

    public void setCarsTypesState(int carsTypeVisibility) {
        this.carsTypesState.set(carsTypeVisibility);
    }

    public ObservableField<Integer> getCarsTypesErrorView() {
        return carsTypesErrorView;
    }

    public void setCarsTypesErrorView(int carsTypesErrorView) {
        this.carsTypesErrorView.set(carsTypesErrorView);
    }

    public ObservableField<String> getCarsTypesErrorMessage() {
        return carsTypesErrorMessage;
    }

    public void setCarsTypesErrorMessage(String carsTypesErrorMessage) {
        this.carsTypesErrorMessage.set(carsTypesErrorMessage);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean buttonsClickable) {
        this.buttonsClickable.set(buttonsClickable);
    }

    public ObservableField<Double> getDistance() {
        return distance;
    }

    // Setters & Getters --- end --- \\

    protected interface ViewListener {
        void showToastMessage(String message);

        void clearPlacesAdapter();

        void setLocationAddress(EditText editText, String addressName);

        void drawRoute(PolylineOptions polylineOptions, LatLng pickup, LatLng destination);

        void removeRoute();

        void startFetchingCarsTypes();
    }
}
