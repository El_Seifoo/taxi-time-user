package com.taxi_time_user.app.main.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.FragmentMapBinding;
import com.taxi_time_user.app.models.Car;
import com.taxi_time_user.app.models.PlaceObj;

import java.util.List;

public class MapFragment extends Fragment implements OnMapReadyCallback, PlacesAdapter.PlaceItemClickListener, MapViewModel.ViewListener, MapPresenter, CarsTypesAdapter.onCarsItemClicked {
    private FragmentMapBinding binding;
    private MapViewModel viewModel;
    private BottomSheetBehavior bottomSheetBehavior;
    private RecyclerView placesList;
    private PlacesAdapter placesAdapter;


    private GoogleMap googleMap;

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);
        viewModel = new ViewModelProvider(this).get(MapViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        // initialize map
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // initializing bottom sheet
        bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet.mapPickupDestinationBottomSheet);

        // setting the visible part of bottom sheet while collapsing
        final View view = binding.getRoot();
        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                binding.bottomSheet.whiteView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                bottomSheetBehavior.setPeekHeight(binding.bottomSheet.whiteView.getMeasuredHeight());
            }
        });

        // initialize places recycler view
        placesList = binding.bottomSheet.placesList;
        placesList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        placesAdapter = new PlacesAdapter(this);


        // req. places api to fetch places
        viewModel.requestPlaces(" ").observe(getViewLifecycleOwner(), new Observer<List<PlaceObj>>() {
            @Override
            public void onChanged(List<PlaceObj> placeObjs) {
                placesAdapter.setPlaces(placeObjs);
                placesList.setAdapter(placesAdapter);
            }
        });

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    @Override
    public void onPlaceItemClicked(PlaceObj placeObj) {
        viewModel.handleOnPlaceItemAction(placeObj, binding.bottomSheet.pickupLocation, binding.bottomSheet.destinationLocation);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getContext().getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void clearPlacesAdapter() {
        placesAdapter.clear();
    }

    // set pickup/destination editText address
    @Override
    public void setLocationAddress(EditText editText, String addressName) {
        editText.setText(addressName);
    }

    private Polyline polyline;

    @Override
    public void drawRoute(PolylineOptions polylineOptions, LatLng pickup, LatLng destination) {
        googleMap.clear();

        googleMap.addMarker(new MarkerOptions()
                .position(pickup))
                .setIcon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_current_location_icon));
        googleMap.addMarker(new MarkerOptions()
                .position(destination))

                .setIcon(bitmapDescriptorFromVector(getContext(), R.drawable.ic_pick_location_icon));
        polyline = googleMap.addPolyline(polylineOptions);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(destination, 16);
        googleMap.animateCamera(cameraUpdate);
    }

    @Override
    public void removeRoute() {
        googleMap.clear();
        if (polyline != null)
            polyline.remove();
        binding.bottomSheet.destinationLocation.setText("");
    }

    @Override
    public void startFetchingCarsTypes() {
        RecyclerView carsTypesList = binding.carsTypesLayout.carsTypesList;
        carsTypesList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        final CarsTypesAdapter carsTypesAdapter = new CarsTypesAdapter(this);
        carsTypesList.setAdapter(carsTypesAdapter);
        viewModel.requestCarsTypes().observe(this, new Observer<List<Car>>() {
            @Override
            public void onChanged(List<Car> cars) {
                carsTypesAdapter.setCars(cars);
                binding.setSelectedCar(cars.get(0));
            }
        });
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(0, 0, 0, 0);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onBackIconClicked(int index) {
        viewModel.handleOnBackIconAction(index);
    }

    @Override
    public void onRetryRequests(int index) {
        viewModel.handleOnRetryAction(index);
    }

    @Override
    public void onRequestButtonClicked() {
        viewModel.requestOrder(binding.getSelectedCar().getId());
    }

    @Override
    public void onCarClickListener(Car car) {
        binding.setSelectedCar(car);
    }
}
