package com.taxi_time_user.app.main.map;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.PlacesListItemBinding;
import com.taxi_time_user.app.models.PlaceObj;

import java.util.List;

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.Holder> {
    private List<PlaceObj> places;
    private final PlaceItemClickListener listener;

    public PlacesAdapter(PlaceItemClickListener listener) {
        this.listener = listener;
    }

    public void setPlaces(List<PlaceObj> places) {
        this.places = places;
        notifyDataSetChanged();
    }

    public void clear() {
        if (places != null) {
            places.clear();
            notifyDataSetChanged();
        }
    }

    protected interface PlaceItemClickListener {
        void onPlaceItemClicked(PlaceObj placeObj);
    }


    @NonNull
    @Override
    public PlacesAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((PlacesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.places_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PlacesAdapter.Holder holder, int position) {
        holder.binding.setPlace(places.get(position));
    }

    @Override
    public int getItemCount() {
        return places != null ? places.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        PlacesListItemBinding binding;

        public Holder(@NonNull PlacesListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onPlaceItemClicked(binding.getPlace());
        }
    }
}
