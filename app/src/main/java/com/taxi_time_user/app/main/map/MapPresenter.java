package com.taxi_time_user.app.main.map;

public interface MapPresenter {
    void onBackIconClicked(int index);

    void onRetryRequests(int index);

    void onRequestButtonClicked();
}
