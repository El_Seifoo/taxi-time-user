package com.taxi_time_user.app.main;

import android.app.Application;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.AndroidViewModel;

import com.taxi_time_user.app.history.HistoryActivity;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.invite.InviteActivity;
import com.taxi_time_user.app.main.map.MapFragment;
import com.taxi_time_user.app.notifications.NotificationsActivity;
import com.taxi_time_user.app.settings.SettingsActivity;
import com.taxi_time_user.app.wallet.WalletActivity;

public class MainViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator betn. view model and its view */

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    protected void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    /*
         check if activity is created for the first time or not ..
         if true --> load home fragment
     */
    protected void checkSavedInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
            viewListener.loadHomeFragment();
        }
    }

    /*
        handle action of all navDrawer items
     */
    protected boolean handleOnNavigationItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_wallet:
                return viewListener.navigateTo(WalletActivity.class);
            case R.id.nav_history:
                return viewListener.navigateTo(HistoryActivity.class);
            case R.id.nav_notifications:
                return viewListener.navigateTo(NotificationsActivity.class);
            case R.id.nav_invite_friends:
//                return viewListener.navigateTo(ChatActivity.class);
                return viewListener.navigateTo(InviteActivity.class);
            case R.id.nav_settings:
                return viewListener.navigateTo(SettingsActivity.class);
            case R.id.nav_logout:
                return viewListener.handleLogoutAction(MainActivity.class);
            case R.id.nav_home:
            default:
                // navItemIndex , CURRENT_TAG
                viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
                break;
        }

        viewListener.loadHomeFragment();
        return true;
    }

    /*
        return the requested fragment depending on the param (navItemIndex)
        in our case there is only one fragment so we wont use navItemIndex ..
     */
    protected Fragment getSelectedFragment(int navItemIndex) {
//        in case of adding any new fragments
//        switch (naveItemIndex) {
//            case 0:
//            default:
//                return MapFragment.newInstance();
//        }

        return MapFragment.newInstance();
    }

    /*
        handle on back button pressed ..
        if drawer is open -> close it .
        else if the current fragment is not the main fragment -> back to main fragment
                (this case wont happen till now because we have only on fragment)
        else do the default action of back button
     */
    public boolean onBackBtnPressed(boolean drawerOpen, boolean shouldLoadHomeFragOnBackPress) {
        if (drawerOpen) {
            viewListener.closeDrawer();
            return true;
        }

        // return to home fragment if user pressed back in other fragment
        if (shouldLoadHomeFragOnBackPress) {
            viewListener.handleNavFragments(0, MainActivity.TAG_HOME, false);
            viewListener.loadHomeFragment();
            return true;
        }

        return false;
    }


    protected interface ViewListener {
        void loadHomeFragment();

        boolean navigateTo(Class destination);

        boolean handleLogoutAction(Class destination);

        void closeDrawer();

        void handleNavFragments(int naveItemIndex, String currentTag, boolean shouldLoadHomeFragOnBackPress);
    }
}
