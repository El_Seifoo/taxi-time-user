package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("_id")
    private String id;
    @SerializedName("country")
    private String name;
    @SerializedName("currency")
    private String currency;
    @SerializedName("promoCodeBonus")
    private String promoCode;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPromoCode() {
        return promoCode;
    }
}
