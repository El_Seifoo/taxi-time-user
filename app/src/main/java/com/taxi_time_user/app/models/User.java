package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("name")
    private String username;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("email")
    private String email;
    @SerializedName("promocode")
    private String promoCode;
    @SerializedName("type")
    private int type;
    @SerializedName("active")
    private boolean active;
    @SerializedName("image")
    private String photo;

    public User(String username, String phoneNumber) {
        this.username = username;
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public int getType() {
        return type;
    }

    public boolean isActive() {
        return active;
    }

    public String getPhoto() {
        return photo;
    }
}
