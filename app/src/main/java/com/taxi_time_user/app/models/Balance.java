package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class Balance {
    @SerializedName("balance")
    private double balance;
    @SerializedName("currency")
    private String currency;

    public Balance(double balance, String currency) {
        this.balance = balance;
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public String getCurrency() {
        return currency;
    }
}
