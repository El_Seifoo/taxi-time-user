package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Sign implements Serializable {

    @SerializedName("name")
    private String username;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("user_type")
    private final int userType = 1;// user type 1 -> user not driver
    @SerializedName("fcm_token")
    private String fcmToken;
    @SerializedName("country")
    private String countryId;
    @SerializedName("faceId")
    private String faceId;
    @SerializedName("email")
    private String email;

    // register
    public Sign(String username,String email, String phoneNumber,  String fcmToken, String countryId) {
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.fcmToken = fcmToken;
        this.countryId = countryId;
    }

    // login
    public Sign(String phoneNumber, String fcmToken) {
        this.phoneNumber = phoneNumber;
        this.fcmToken = fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getUserType() {
        return userType;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public String getCountryId() {
        return countryId;
    }

    public String getFaceId() {
        return faceId;
    }
}
