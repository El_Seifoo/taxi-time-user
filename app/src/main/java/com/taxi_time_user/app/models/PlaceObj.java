package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class PlaceObj {
    @SerializedName("place_id")
    private String placeId;
    @SerializedName("structured_formatting")
    private PlaceName placeName;

    public String getPlaceId() {
        return placeId;
    }

    public PlaceName getPlaceName() {
        return placeName;
    }

    public class PlaceName {
        @SerializedName("main_text")
        private String mainName;
        @SerializedName("secondary_text")
        private String secName;

        public String getMainName() {
            return mainName;
        }

        public String getSecName() {
            return secName;
        }
    }
}
