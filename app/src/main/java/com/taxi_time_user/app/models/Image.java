package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class Image {
    @SerializedName("imageId")
    private String imageId;
    @SerializedName("format")
    private String format;
    private final static String root = "";

    public String getImageId() {
        return imageId;
    }

    public String getFormat() {
        return format;
    }
}
