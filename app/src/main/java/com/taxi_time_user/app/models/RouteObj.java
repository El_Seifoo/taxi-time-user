package com.taxi_time_user.app.models;

public class RouteObj {
    private String startName, endName;
    private double startLat, startLng, endLat, endLng;
    private String overviewPolyline;

    public RouteObj(String startName, String endName, double startLat,
                    double startLng, double endLat, double endLng, String overviewPolyline) {
        this.startName = startName;
        this.endName = endName;
        this.startLat = startLat;
        this.startLng = startLng;
        this.endLat = endLat;
        this.endLng = endLng;
        this.overviewPolyline = overviewPolyline;
    }

    public String getStartName() {
        return startName;
    }

    public String getEndName() {
        return endName;
    }

    public double getStartLat() {
        return startLat;
    }

    public double getStartLng() {
        return startLng;
    }

    public double getEndLat() {
        return endLat;
    }

    public double getEndLng() {
        return endLng;
    }

    public String getOverviewPolyline() {
        return overviewPolyline;
    }
}
