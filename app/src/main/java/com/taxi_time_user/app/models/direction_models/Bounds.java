
package com.taxi_time_user.app.models.direction_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bounds {

    @SerializedName("northeast")
    @Expose
    private LatLngs northeast;
    @SerializedName("southwest")
    @Expose
    private LatLngs southwest;

    public LatLngs getNortheast() {
        return northeast;
    }

    public void setNortheast(LatLngs northeast) {
        this.northeast = northeast;
    }

    public LatLngs getSouthwest() {
        return southwest;
    }

    public void setSouthwest(LatLngs southwest) {
        this.southwest = southwest;
    }

}
