package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class Order {
    public void setRequestTime(long requestTime) {
        this.requestTime = requestTime;
    }

    @SerializedName("orderId")
    private String id;
    @SerializedName("fromAddress")
    private String fromAddress;
    @SerializedName("toAddress")
    private String toAddress;
    @SerializedName("driverName")
    private String driverName;
    @SerializedName("riderName")
    private String riderName;
    @SerializedName("carType")
    private String carType;
    @SerializedName("carPlateNumber")
    private String carPlateNumber;
    @SerializedName("distance")
    private double distance;
    @SerializedName("requestTripTime")
    private long requestTime;
    @SerializedName("status")
    private int status;
    @SerializedName("statusMessage")
    private String statusMessage;
    @SerializedName("currency")
    private String currency;
    @SerializedName("fare")
    private double fare;

    public String getId() {
        return id;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public String getToAddress() {
        return toAddress;
    }

    public String getDriverName() {
        return driverName;
    }

    public String getRiderName() {
        return riderName;
    }

    public String getCarType() {
        return carType;
    }

    public String getCarPlateNumber() {
        return carPlateNumber;
    }

    public double getDistance() {
        return distance;
    }

    public long getRequestTime() {
        return requestTime;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public String getCurrency() {
        return currency;
    }

    public double getFare() {
        return fare;
    }
}
