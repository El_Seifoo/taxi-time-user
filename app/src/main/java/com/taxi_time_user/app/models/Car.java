package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class Car {
    @SerializedName("_id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("kiloPrice")
    private double pricePerKilo;
    @SerializedName("waitingPrice")
    private double waitingPrice;
    @SerializedName("icon")
    private Image image;
    @SerializedName("currency")
    private String currency;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getPricePerKilo() {
        return pricePerKilo;
    }

    public double getWaitingPrice() {
        return waitingPrice;
    }

    public Image getImage() {
        return image;
    }

    public String getCurrency() {
        return currency;
    }
}
