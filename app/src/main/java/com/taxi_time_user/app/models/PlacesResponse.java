package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlacesResponse {
    @SerializedName("predictions")
    private List<PlaceObj> places;
    @SerializedName("status")
    private String status;

    public List<PlaceObj> getPlaces() {
        return places;
    }

    public String getStatus() {
        return status;
    }
}
