package com.taxi_time_user.app.models;

import com.google.gson.annotations.SerializedName;

public class ApiResponse<S> {
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName(value = "data", alternate = {"token", "cars", "orderId","user"})
    private S data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public S getData() {
        return data;
    }
}
