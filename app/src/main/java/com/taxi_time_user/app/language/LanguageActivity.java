package com.taxi_time_user.app.language;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.jaeger.library.StatusBarUtil;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityLanguageBinding;
import com.taxi_time_user.app.databinding.ActivitySplashBinding;
import com.taxi_time_user.app.others.ActivityHelper;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

public class LanguageActivity extends AppCompatActivity {

    private ActivityLanguageBinding binding;
    String lang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_language);
        binding.btnArabic.setOnClickListener(this::onArabicClicked);
        binding.btnEnglish.setOnClickListener(this::onEnglishClicked);
        binding.btnAction.setOnClickListener(this::onActionClicked);
        binding.btnAction.getBackground().setColorFilter(getResources().getColor(R.color.grey), PorterDuff.Mode.SRC_IN);


    }

    private void onArabicClicked(View view) {
        englishSelected(false);

    }
    private void onEnglishClicked(View view) {
        englishSelected(true);
    }

    void englishSelected(Boolean b){
        binding.btnAction.setEnabled(true);
        binding.btnAction.getBackground().setColorFilter(getResources().getColor(R.color.light_blue), PorterDuff.Mode.SRC_IN);
        if(b){
            lang = "en";
            binding.btnEnglish.setTextColor(getResources().getColor(R.color.red));
            binding.btnArabic.setTextColor(getResources().getColor(R.color.black));
        }else {
            lang = "ar";
            binding.btnEnglish.setTextColor(getResources().getColor(R.color.black));
            binding.btnArabic.setTextColor(getResources().getColor(R.color.red));
        }
    }

    private void onActionClicked(View view) {
        MySingleton.getInstance(this).saveStringToSharedPref(Constants.APP_LANGUAGE, lang);
        ActivityHelper.startSignActivity(this,true);

    }


}
