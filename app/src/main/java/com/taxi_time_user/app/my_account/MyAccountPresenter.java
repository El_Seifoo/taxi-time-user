package com.taxi_time_user.app.my_account;

public interface MyAccountPresenter {
    void onClick(int index);

    void onProfPicClicked();
}
