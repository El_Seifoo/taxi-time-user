package com.taxi_time_user.app.my_account;

public interface BottomSheetPresenter {
    void onSaveClicked();

    void onCancelClicked();
}