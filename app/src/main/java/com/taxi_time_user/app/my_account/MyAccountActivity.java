package com.taxi_time_user.app.my_account;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityMyAccountBinding;
import com.taxi_time_user.app.models.User;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

public class MyAccountActivity extends AppCompatActivity implements MyAccountViewModel.ViewListener, MyAccountPresenter, EditTextBottomSheet.ParentCommunicator, PhoneNumberBottomSheet.ParentCommunicator {
    protected static final int PICK_PICTURE_REQUEST_CODE = 1;
    private ActivityMyAccountBinding binding;
    private MyAccountViewModel viewModel;
    private EditTextBottomSheet editTextBottomSheet;
    private PhoneNumberBottomSheet phoneNumberBottomSheet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_account);
        viewModel = new ViewModelProvider(this).get(MyAccountViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);
        binding.setUser((User) getIntent().getExtras().getSerializable("User"));
        viewModel.setPickedPicture(binding.getUser().getPhoto());

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        actionBar.setElevation(0);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void fieldUpdatedSuccessfully(String tag) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment instanceof EditTextBottomSheet)
            ((EditTextBottomSheet) fragment).dismiss();
        else if (fragment instanceof PhoneNumberBottomSheet)
            ((PhoneNumberBottomSheet) fragment).dismiss();
    }

    @Override
    public void showEditTextBottomSheet(String data, String tag) {
        editTextBottomSheet = EditTextBottomSheet.newInstance(tag, data);
        editTextBottomSheet.show(getSupportFragmentManager(), tag);
    }


    @Override
    public void showPhoneBottomSheet() {
        phoneNumberBottomSheet = PhoneNumberBottomSheet.newInstance();
        phoneNumberBottomSheet.show(getSupportFragmentManager(), "PhoneNumber");
    }

    @Override
    public void startPicking(Intent intent, int requestCode) {
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void startCroppingFrag(Uri uri) {
        CropImage.activity(uri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(1, 1)
                .start(this);
    }

    @Override
    public String getName() {
        return binding.getUser().getUsername();
    }

    @Override
    public String getEmail() {
        return binding.getUser().getEmail();
    }

    @Override
    public void onClick(int index) {
        viewModel.onItemClicked(index);
    }

    @Override
    public void onProfPicClicked() {
        viewModel.tag = "";
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PICK_PICTURE_REQUEST_CODE
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSaveClicked(String string, String tag) {
        viewModel.requestUpdateUser(string, tag);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
