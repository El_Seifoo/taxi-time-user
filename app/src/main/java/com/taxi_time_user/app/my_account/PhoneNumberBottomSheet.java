package com.taxi_time_user.app.my_account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rilixtech.CountryCodePicker;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.PhoneNumberBottomSheetLayoutBinding;

public class PhoneNumberBottomSheet extends BottomSheetDialogFragment implements BottomSheetPresenter {
    private PhoneNumberBottomSheetLayoutBinding binding;
    private ParentCommunicator parentListener;
    private CountryCodePicker countryCodePicker;

    public static PhoneNumberBottomSheet newInstance() {
        return new PhoneNumberBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater, R.layout.phone_number_bottom_sheet_layout, container, false);
        binding.setPresenter(this);


        countryCodePicker = binding.countryCodePicker;
        countryCodePicker.registerPhoneNumberTextView(binding.phoneEditText);

        parentListener = (ParentCommunicator) getContext();

        return binding.getRoot();
    }

    @Override
    public void onSaveClicked() {
        parentListener.onSaveClicked(countryCodePicker.getSelectedCountryCodeWithPlus() + "/" + binding.phoneEditText.getText().toString().trim(), "PhoneNumber");
    }

    @Override
    public void onCancelClicked() {
        dismiss();
    }

    public interface ParentCommunicator {
        void onSaveClicked(String string, String tag);
    }
}
