package com.taxi_time_user.app.my_account;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.User;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class MyAccountViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator betn. view model and its view */
    private ObservableField<Boolean> buttonsClickable;/* flag to disable or enable buttons */
    protected ObservableField<Integer> progress;/* flag to know the status of progress dialog */
    private ObservableField<String> pickedPicture/* flag to carry the picked profile picture before start uploading */;

    public MyAccountViewModel(@NonNull Application application) {
        super(application);
        buttonsClickable = new ObservableField<>(true);
        progress = new ObservableField<>(View.GONE);
        pickedPicture = new ObservableField<>("");
    }


    public void onItemClicked(int index) {
        switch (index) {
            case 1:
                tag = "";
                String name = viewListener.getName();
                viewListener.showEditTextBottomSheet(name == null ? "" : name, "Name");
                break;
            case 2:
                String email = viewListener.getEmail();
                viewListener.showEditTextBottomSheet(email == null ? "" : email, "Email");
                break;
            case 5:
                tag = "";
                viewListener.showPhoneBottomSheet();

        }
    }

    protected String tag;

    /*
        check inputs to update user data depending on tag ..
        @param tag .. flag to know which data is going to be updated
     */
    protected void requestUpdateUser(String fieldString, String tag) {
        if (tag.equals("Name") || tag.equals("Email"))
            if (fieldString.isEmpty()) {
                viewListener.showToastMessage(tag.concat(" ").concat(getApplication().getString(R.string.blank_error)));
                return;
            }


        if (tag.equals("Email"))
            if (!checkMailValidation(fieldString)) {
                viewListener.showToastMessage(getApplication().getString(R.string.mail_not_valid));
                return;
            }

        // i used / to separate betn code and phone number .. if string end with / then phone number is not entered
        // else phone number is entered with code
        if (tag.equals("PhoneNumber"))
            if (fieldString.trim().charAt(fieldString.length() - 1) == '/') {
                viewListener.showToastMessage(tag.concat(" ").concat(getApplication().getString(R.string.blank_error)));
                return;
            }

//        setProgress(View.VISIBLE);

        this.tag = tag;
        Map<String, String> header = new HashMap<>();
        header.put("lang", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.english_key)));
        header.put("Authorization", MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));

        Map<String, String> body = new HashMap<>();
        body.put(tag.equals("PhoneNumber") ? "phoneNumber" : tag.toLowerCase(), tag.equals("PhoneNumber") ? fieldString.replace("/", "") : fieldString);
        updateProfile(body);
    }


    /*
        checking email validation using regular expression ..
     */
    public static boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        return matcher.matches();
    }

    /*
        check result of requesting permissions
     */
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //check if request is picking pic req
        if (requestCode == MyAccountActivity.PICK_PICTURE_REQUEST_CODE) {
            // if request is approved make user pick his picture from gallery
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                viewListener.startPicking(Intent.createChooser(intent, getApplication().getString(R.string.profile_picture)), MyAccountActivity.PICK_PICTURE_REQUEST_CODE);
            }
        }
    }

    /*
        check activity result for picking and cropping profile pic
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if request for pick profile picture from gallery
        if (requestCode == MyAccountActivity.PICK_PICTURE_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            viewListener.startCroppingFrag(data.getData());
            return;
        }

        // if request for cropping image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                String img;
                Cursor cursor = getApplication().getContentResolver().query(result.getUri(), null, null, null, null);
                if (cursor == null) {
                    img = result.getUri().getPath();
                } else {
                    cursor.moveToFirst();
                    int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    img = cursor.getString(idx);
                    cursor.close();
                }
                setPickedPicture(img);
                //convert to base64
//                Log.e("encodedImage", "data:image/jpeg;base64," + encodeImage(img));
                Map<String, String> body = new HashMap<>();
                body.put("image", "data:image/jpeg;base64," + encodeImage(img));
                updateProfile(body);
            }
        }
    }

    private String encodeImage(String img) {
        /*
            InputStream inputStream = getApplication().getContentResolver().openInputStream(Uri.fromFile(new File(photo)));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(inputStream), 100, 100, true);
            bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            return Base64.encodeToString(imageBytes, Base64.NO_WRAP);
         */
        String base64Image = "";
        File file = new File(img);
        try (FileInputStream imageInFile = new FileInputStream(file)) {
            byte[] imageDate = new byte[(int) file.length()];
            imageInFile.read(imageDate);
            base64Image = Base64.encodeToString(imageDate, Base64.DEFAULT);
        } catch (FileNotFoundException e) {
            Log.e("fileNotFound", e.toString());
        } catch (IOException ioe) {
            Log.e("IOE", ioe.toString());
        }
        return base64Image;
    }

    private void updateProfile(Map<String, String> body) {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        Call<ApiResponse<Void>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .updateProfile(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language))
                        , MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""),
                        body);

        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                setProgress(View.GONE);
                setButtonsClickable(true);

                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<Void> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        viewListener.showToastMessage((tag.equals("Name") ? getApplication().getString(R.string.username) :
                                tag.equals("Email") ? getApplication().getString(R.string.email) :
                                        tag.equals("PhoneNumber") ? getApplication().getString(R.string.phone_number) :
                                                getApplication().getString(R.string.profile_picture)).concat(" ")
                                .concat(getApplication().getString(R.string.updated_successfully)));
                        viewListener.fieldUpdatedSuccessfully(tag);
                    } else {
                        viewListener.showToastMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        viewListener.showToastMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                viewListener.showToastMessage(getExceptionError(t));

            }
        });
    }

    private String getExceptionError(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }


    // Setters & Getters  ---------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<String> getPickedPicture() {
        return pickedPicture;
    }

    public void setPickedPicture(String pickedPicture) {
        this.pickedPicture.set(pickedPicture);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }// Setters & Getters  ---------> end


    protected interface ViewListener {
        void showToastMessage(String message);

        void fieldUpdatedSuccessfully(String tag);

        void showEditTextBottomSheet(String data, String tag);

        void showPhoneBottomSheet();

        void startPicking(Intent intent, int requestCode);

        void startCroppingFrag(Uri uri);

        String getName();

        String getEmail();
    }
}
