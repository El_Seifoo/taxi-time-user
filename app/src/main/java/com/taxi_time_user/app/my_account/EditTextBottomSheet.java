package com.taxi_time_user.app.my_account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.EditTextBottomSheetLayoutBinding;

public class EditTextBottomSheet extends BottomSheetDialogFragment implements BottomSheetPresenter {
    private EditTextBottomSheetLayoutBinding binding;
    private ParentCommunicator parentListener;/* communicator between bottom sheet and its parent (account activity) */

    public static EditTextBottomSheet newInstance(String label, String data) {
        EditTextBottomSheet bottomSheet = new EditTextBottomSheet();

        Bundle bundle = new Bundle();
        bundle.putString("label", label);
        bundle.putString("data", data);
        bottomSheet.setArguments(bundle);
        return bottomSheet;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.edit_text_bottom_sheet_layout, container, false);
        binding.setPresenter(this);
        binding.setData(getArguments().getString("data"));
        binding.setLabel(getArguments().getString("label"));

        parentListener = (ParentCommunicator) getContext();
        return binding.getRoot();
    }

    @Override
    public void onSaveClicked() {
        parentListener.onSaveClicked(binding.editText.getText().toString(), getTag());
    }

    @Override
    public void onCancelClicked() {
        dismiss();
    }

    protected interface ParentCommunicator {
        void onSaveClicked(String string, String tag);
    }

}
