package com.taxi_time_user.app.terms;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.taxi_time_user.app.others.TermsContactUs;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsViewModel extends AndroidViewModel {
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<String> errorMessage;/* error message of terms request */
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */

    public TermsViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        req. fetching app terms & conditions
     */
    private MutableLiveData<TermsContactUs> termsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<TermsContactUs> requestTerms() {
        if (termsMutableLiveData.getValue() == null) {
            loadTerms();
        }

        return termsMutableLiveData;
    }

    private void loadTerms() {
        setProgress(View.VISIBLE);
        Call<ApiResponse<List<TermsContactUs>>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .term(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language)));

        call.enqueue(new Callback<ApiResponse<List<TermsContactUs>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<TermsContactUs>>> call, Response<ApiResponse<List<TermsContactUs>>> response) {
                setProgress(View.GONE);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<List<TermsContactUs>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        List<TermsContactUs> terms = apiResponse.getData();
                        if (terms == null) {
                            showResponseMessage(apiResponse.getMessage());
                        } else if (terms.isEmpty()) {
                            showResponseMessage(apiResponse.getMessage());
                        } else {
                            termsMutableLiveData.setValue(terms.get(0));
                        }
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<TermsContactUs>>> call, Throwable t) {
                setProgress(View.GONE);
                onFailureHandler(t);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        termsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request terms again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        termsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request terms again
        showResponseMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }


    // Setters & Getters --- start --- \\
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setters & Getters --- end --- \\
}
