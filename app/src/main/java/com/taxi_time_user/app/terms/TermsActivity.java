package com.taxi_time_user.app.terms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityTermsBinding;
import com.taxi_time_user.app.others.TermsContactUs;

public class TermsActivity extends AppCompatActivity implements TermsPresenter {
    private ActivityTermsBinding binding;
    private TermsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms);
        viewModel = new ViewModelProvider(this).get(TermsViewModel.class);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);
        binding.setTerms("");

        viewModel.requestTerms().observe(this, new Observer<TermsContactUs>() {
            @Override
            public void onChanged(TermsContactUs terms) {
                binding.setTerms(terms.getTerms());
            }
        });
    }

    @Override
    public void onRetryClicked() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestTerms();
    }
}
