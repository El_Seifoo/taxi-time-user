package com.taxi_time_user.app.notifications;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.NotificationListItemBinding;
import com.taxi_time_user.app.models.Notification;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.Holder> {
    private List<Notification> notifications;

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NotificationsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((NotificationListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.notification_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsAdapter.Holder holder, int position) {
        holder.binding.setNotification(notifications.get(position));
    }

    @Override
    public int getItemCount() {
        return notifications != null ? notifications.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder {
        private NotificationListItemBinding binding;

        public Holder(@NonNull NotificationListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
