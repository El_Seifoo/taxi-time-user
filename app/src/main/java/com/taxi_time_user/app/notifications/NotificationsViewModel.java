package com.taxi_time_user.app.notifications;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Notification;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsViewModel extends AndroidViewModel {
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<Integer> emptyListTextView;/* flag to know if the notifications list is empty or not */
    private ObservableField<String> errorMessage;/* error message of countries request */
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */

    public NotificationsViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        emptyListTextView = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        fetch all available notifications
     */
    private MutableLiveData<List<Notification>> notificationsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<List<Notification>> requestNotifications() {
        if (notificationsMutableLiveData.getValue() == null) {
            loadNotifications();
        }

        return notificationsMutableLiveData;
    }

    private void loadNotifications() {
        setProgress(View.VISIBLE);

        Call<ApiResponse<List<Notification>>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .notifications(MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication()
                                        .getString(R.string.default_language)),
                        MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.USER_TOKEN, ""));

        call.enqueue(new Callback<ApiResponse<List<Notification>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Notification>>> call, Response<ApiResponse<List<Notification>>> response) {
                setProgress(View.GONE);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<List<Notification>> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        List<Notification> notifications = apiResponse.getData();
                        if (notifications == null) {
                            setEmptyListTextView(View.VISIBLE);// there are no notifications fetched
                            notificationsMutableLiveData.setValue(new ArrayList<Notification>());
                        } else if (notifications.isEmpty()) {
                            setEmptyListTextView(View.VISIBLE);// there are no notifications fetched
                            notificationsMutableLiveData.setValue(notifications);
                        } else {// fetch notifications and set to the list
                            setEmptyListTextView(View.GONE);
                            notificationsMutableLiveData.setValue(notifications);
                        }
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Notification>>> call, Throwable t) {
                setProgress(View.GONE);
                onFailureHandler(t);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        notificationsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request notifications again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        notificationsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request notifications again
        showResponseMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    // Setters & Getters --- start --- \\
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<Integer> getEmptyListTextView() {
        return emptyListTextView;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public void setEmptyListTextView(int empty) {
        this.emptyListTextView.set(empty);
    }
    // Setters & Getters --- end --- \\
}
