package com.taxi_time_user.app.notifications;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityNotificationsBinding;
import com.taxi_time_user.app.models.Notification;

import java.util.List;

public class NotificationsActivity extends AppCompatActivity implements NotificationsPresenter {
    //test
    private ActivityNotificationsBinding binding;
    private NotificationsViewModel viewModel;
    private RecyclerView notificationsList;
    private NotificationsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notifications);
        viewModel = new ViewModelProvider(this).get(NotificationsViewModel.class);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);


        notificationsList = binding.notificationsList;
        notificationsList.setLayoutManager(new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false));
        adapter = new NotificationsAdapter();
        notificationsList.setAdapter(adapter);

        viewModel.requestNotifications().observe(this, new Observer<List<Notification>>() {
            @Override
            public void onChanged(List<Notification> notifications) {
                adapter.setNotifications(notifications);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getString(R.string.notifications));
    }

    @Override
    public void onRetry() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestNotifications();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
