package com.taxi_time_user.app.others;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.shawnlin.numberpicker.NumberPicker;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.main.map.MapViewModel;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class BindingUtils {

    public static String checkNullability(String data) {
        return data == null ? "" : data;
    }

    @BindingAdapter({"android:fare", "android:currency"})
    public static void orderFareWithCurrency(TextView textView, double fare, String currency) {
        textView.setText((fare + " ").concat(currency == null ? " " : currency));
    }

    /*
        set bottom sheet label
     */
    @BindingAdapter("android:bottom_sheet_label")
    public static void editTextBottomSheetLabel(TextView textView, String label) {
        textView.setText(textView.getContext().getString(R.string.edit_text_bottom_sheet_label).concat(" ").concat(label));
    }

    /*
        update ui of login activity when user change from register to login or opposite
     */
    @BindingAdapter("android:sign_status")
    public static void setSignStatus(View view, boolean isRegister) {
        switch (view.getId()) {
            case R.id.sign_up_text:
                ((TextView) view).setTextColor(isRegister ? view.getContext().getResources().getColor(R.color.black) :
                        view.getContext().getResources().getColor(R.color.grey_chateau));
                break;
            case R.id.sign_up_underline:
                view.setVisibility(isRegister ? View.VISIBLE : View.INVISIBLE);
                break;
            case R.id.sign_in_text:
                ((TextView) view).setTextColor(!isRegister ? view.getContext().getResources().getColor(R.color.black) :
                        view.getContext().getResources().getColor(R.color.grey_chateau));
                break;
            case R.id.sign_in_underline:
                view.setVisibility(!isRegister ? View.VISIBLE : View.INVISIBLE);
                break;
            case R.id.login_label:
                ((TextView) view).setVisibility(isRegister ? View.GONE : View.VISIBLE);
                break;
            case R.id.username_edit_text:
                ((EditText) view).setVisibility(isRegister ? View.VISIBLE : View.GONE);
                break;
            case R.id.sign_button:
                ((Button) view).setText(isRegister ? view.getContext().getText(R.string.sign_up) : view.getContext().getText(R.string.next));
                break;
            case R.id.facebook_login_container:
            case R.id.country_container:
                ((LinearLayout) view).setVisibility(isRegister ? View.VISIBLE : View.GONE);
                break;
        }
    }

    /*
        append country currency to country name in countries list
     */
    @BindingAdapter({"android:set_country", "android:set_currency"})
    public static void setCountry(TextView view, String countryName, String currency) {
        view.setText(countryName.concat(" (").concat(currency).concat(")"));
    }


    /*
        set - pickup,destination - bottom sheet state
     */
    @BindingAdapter("android:bottom_sheet_state")
    public static void setState(View container, int state) {
        BottomSheetBehavior behavior = BottomSheetBehavior.from(container);

        if (state == BottomSheetBehavior.STATE_HIDDEN)
            behavior.setHideable(true);
        else // if state is not STATE_HIDDEN disable hidden feature of bottom sheet to make user able to pick pickup,destination locations
            behavior.setHideable(false);
        behavior.setState(state);
    }

    @BindingAdapter("android:view_model")
    public static void addChange(View container, final MapViewModel viewModel) {
        BottomSheetBehavior behavior = BottomSheetBehavior.from(container);

        behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View view, int state) {
                viewModel.setBottomSheetState(state);
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }


    /*
        set editTexts of - pick,destination - bottom sheet change listener
        when user put any character make req. to fetch places
     */
    @BindingAdapter({"android:view_model", "android:is_from_where"})
    public static void setPickUpDestinationEditTextChangeListener(final EditText editText, final MapViewModel viewModel, boolean isFromWhere) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    viewModel.setBottomSheetState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(editText.getContext().getString(R.string.my_location)))
                    viewModel.requestPlaces(s.toString());
            }
        });
    }

    /*
        calculate expected fare
     */
    @BindingAdapter({"android:distance", "android:fare_per_km", "android:currency"})
    public static void calcExpectedFare(TextView textView, double distance, double farePerKm, String currency) {
        Log.e("distance,FarePerKm", distance + ",,,,,," + farePerKm);
        if (distance != -1 && farePerKm != 0) {
            double expectedFare = distance * farePerKm;
            DecimalFormat df = new DecimalFormat("#.#");
            df.setRoundingMode(RoundingMode.FLOOR);
            String fareInString = df.format(expectedFare);
            Log.e("fareString", fareInString);
            // if number ends with .0 remove dot and zero
            textView.setText((expectedFare == 0 ? "" : expectedFare % 1 == 0 ? fareInString.substring(0, fareInString.indexOf(".")) : fareInString).concat(" " + currency));
        }
    }

    /*
        set app language to textView (Settings activity)
     */
    @BindingAdapter("app_language")
    public static void setLanguage(TextView textView, String fake) {
        textView.setText((MySingleton.getInstance(textView.getContext())
                .getStringFromSharedPref(Constants.APP_LANGUAGE, textView.getContext().getString(R.string.default_language))).equals(
                textView.getContext().getString(R.string.english_key)) ? textView.getContext().getString(R.string.english)
                : textView.getContext().getString(R.string.arabic));
    }

    /*
        load any picture contains root in its url
     */
    @BindingAdapter("android:load_image")
    public static void loadImg(ImageView imageView, String path) {
        Glide.with(imageView.getContext())
                .load(path)
                .thumbnail(0.5f)
                .error(R.mipmap.back_ground)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }

    /*
        load any picture doesn't contain root in its url
     */
    @BindingAdapter("android:load_image_with_root")
    public static void loadImg1(ImageView imageView, String path) {
        Glide.with(imageView.getContext())
                .load(URLs.IMAGES_BASE_URL + path)
                .thumbnail(0.5f)
                .error(R.mipmap.back_ground)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }


    // handle changes of days in feb when year is changing
    @BindingAdapter({"android:months_picker", "android:days_picker", "android:years", "android:days"})
    public static void onYearsPickerValueChanged(NumberPicker yearsPicker, final NumberPicker monthsPicker, final NumberPicker daysPicker, final String[] years, final String[] days) {
        yearsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // if the selected month is Feb
                //          |- if the selected year is divided by 4 then feb has 29 days
                //          |- else then Feb has 28 days
                if (monthsPicker.getValue() == 2) {
                    if (Integer.valueOf(years[newVal - 1]) % 4 == 0) {
                        if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30)
                            daysPicker.setValue(29);
                        daysPicker.setMaxValue(days.length - 2);
                    } else {
                        if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30 || daysPicker.getValue() == 29)
                            daysPicker.setValue(28);
                        daysPicker.setMaxValue(days.length - 3);
                    }
                }
            }
        });

    }


    // handle changing in days depending on changing in months
    @BindingAdapter({"android:days_picker", "android:years_picker", "android:days", "android:years"})
    public static void onMonthsPickerValueChanged(NumberPicker monthsPicker, final NumberPicker daysPicker, final NumberPicker yearsPicker,
                                                  final String[] days, final String[] years) {
        monthsPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                switch (newVal) {
                    // months with 30 days
                    case 4://April
                    case 6://Jun
                    case 9://September
                    case 11://November
                        if (daysPicker.getValue() == 31)
                            daysPicker.setValue(30);
                        daysPicker.setMaxValue(days.length - 1);
                        break;
                    case 2:// february 28/29 days
                        // check if the selected feb has 28 or 29 days
                        if (Integer.valueOf(years[yearsPicker.getValue() - 1]) % 4 == 0) {
                            if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30)
                                daysPicker.setValue(29);
                            daysPicker.setMaxValue(days.length - 2);
                        } else {
                            if (daysPicker.getValue() == 31 || daysPicker.getValue() == 30 || daysPicker.getValue() == 29)
                                daysPicker.setValue(28);
                            daysPicker.setMaxValue(days.length - 3);
                        }
                        break;
                    default://rest of months
                        daysPicker.setMaxValue(days.length);
                }
            }
        });
    }

    // number picker attributes.
    @BindingAdapter("android:value")
    public static void setValue(NumberPicker picker, int value) {
        picker.setValue(value);
    }

    @BindingAdapter("android:max_value")
    public static void setMaxValue(NumberPicker picker, int maxValue) {
        picker.setMaxValue(maxValue);
    }

    @BindingAdapter("android:min_value")
    public static void setMinValue(NumberPicker picker, int minValue) {
        picker.setMinValue(minValue);
    }

    @BindingAdapter("android:values")
    public static void setValues(NumberPicker picker, String[] values) {
        picker.setDisplayedValues(values);
    }
}
