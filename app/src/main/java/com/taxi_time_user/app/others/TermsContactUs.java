package com.taxi_time_user.app.others;

import com.google.gson.annotations.SerializedName;

public class TermsContactUs {
    @SerializedName("terms")
    private String terms;
    @SerializedName("email")
    private String email;
    @SerializedName("phoneNumber")
    private String phone;


    public TermsContactUs(String terms, String email, String phone) {
        this.terms = terms;
        this.email = email;
        this.phone = phone;
    }

    public String getTerms() {
        return terms;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
