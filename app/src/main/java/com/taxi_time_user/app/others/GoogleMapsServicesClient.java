package com.taxi_time_user.app.others;

import com.taxi_time_user.app.models.PlacesResponse;
import com.taxi_time_user.app.models.direction_models.Directions;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface GoogleMapsServicesClient {
    @GET(URLs.PLACES)
    Call<PlacesResponse> fetchPlaces(@Query("input") String query, @Query("key") String apiKey, @Query("language") String lang);

    @GET(URLs.ROUTE)
    Call<Directions> drawRoute(@QueryMap Map<String, String> map);
}
