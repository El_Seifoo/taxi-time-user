package com.taxi_time_user.app.others;

public class Constants {
    public static final String SHARED_PREF_NAME = "taxi_time_user";
    public static final String USER_TOKEN = "token";
    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String APP_LANGUAGE = "app_language";
    public static final String USER_DATA = "user_data";
    public static final String NOTIFICATION_ON_OFF = "notOnOff";
    public static final String COUNTRY_CODE = "country_code";
    public static final String FIREBASE_TOKEN = "firebase_token";
    public static final int google_code = 101;

}
