package com.taxi_time_user.app.others;

public class URLs {
    public static final String ROOT = "http://taxiapp-node.herokuapp.com/api/";
    public static final String MAPS_ROOT = "https://maps.googleapis.com/maps/api/";
    public static final String IMAGES_BASE_URL = "https://res.cloudinary.com/dvuia2xqs/image/upload/";

    public static final String GENERAL = "general/v1/";
    public static final String USER = "user/v1/";
    public static final String CAR = "car/v1/";



    public static final String SIGN_UP_IN = "signup/";
    public static final String PROFILE = "profile/";
    public static final String UPDATE_PROFILE = "update/profile/";
    public static final String COUNTRIES = "get/countries/";
    public static final String CARS_TYPES = "price/";
    public static final String REQUEST_ORDER = "nearby/";
    public static final String CONTACT_US = "contactus/";
    public static final String NOTIFICATIONS = "notifications/log/";
    public static final String HISTORY = "orders/history/";
    public static final String BALANCE = "wallet/balance/";


    public static final String PLACES = "place/autocomplete/json";
    public static final String ROUTE = "directions/json";
}
