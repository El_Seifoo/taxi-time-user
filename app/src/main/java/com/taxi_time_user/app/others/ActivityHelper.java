package com.taxi_time_user.app.others;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;

import com.taxi_time_user.app.language.LanguageActivity;
import com.taxi_time_user.app.main.MainActivity;
import com.taxi_time_user.app.sign.SignActivity;

import java.util.Locale;

public class ActivityHelper {

    public static void startMainActivity(Context context,Boolean cleanTop) {
        Intent intent = new Intent(context, MainActivity.class);
        if(cleanTop)
        cleanTop(intent);
        context.startActivity(intent);
    }
    public static void startSignActivity(Context context,Boolean cleanTop) {
        Intent intent = new Intent(context, SignActivity.class);
        if(cleanTop)
        cleanTop(intent);
        context.startActivity(intent);
    }

    public static void startLanguageActivity(Context context,Boolean cleanTop) {
        Intent intent = new Intent(context, LanguageActivity.class);
        if(cleanTop)
        cleanTop(intent);
        context.startActivity(intent);
    }


    private static void cleanTop(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public static void setLocale(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
