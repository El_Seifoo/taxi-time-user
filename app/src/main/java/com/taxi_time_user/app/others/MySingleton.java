package com.taxi_time_user.app.others;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.taxi_time_user.app.BuildConfig;
import com.taxi_time_user.app.models.User;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MySingleton {
    private static MySingleton mInstance;
    private Retrofit retrofit, retrofitGoogle;
    private Context context;
    private SharedPreferences sharedPreferences;

    private MySingleton(Context context) {
        this.context = context;
        retrofit = getRetrofit(retrofit, 0);
        retrofitGoogle = getRetrofit(retrofitGoogle, 1);
        sharedPreferences = getSharedPreferences();
    }

    private Retrofit getRetrofit(Retrofit retrofit, int index) {
        if (retrofit == null) {
            OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            if (BuildConfig.DEBUG)
                okHttpClient.addInterceptor(loggingInterceptor);

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(index == 0 ? URLs.ROOT : URLs.MAPS_ROOT)
                    .client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create());
            retrofit = builder.build();
        }

        return retrofit;
    }

    public <S> S createService(Class<S> serviceClass, int index) {
        return index == 0 ? retrofit.create(serviceClass) : retrofitGoogle.create(serviceClass);
    }

    public static synchronized MySingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new MySingleton(context);
        }

        return mInstance;
    }

    private SharedPreferences getSharedPreferences() {
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        }
        return sharedPreferences;
    }

    public void saveStringToSharedPref(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getStringFromSharedPref(String key, String defaultValue) {
        return getSharedPreferences().getString(key, defaultValue);
    }

    public void saveBooleanToSharedPref(String key, boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBooleanFromSharedPref(String key, boolean defaultValue) {
        return getSharedPreferences().getBoolean(key, defaultValue);
    }

    public void loginUser() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
    }

    public boolean isLoggedIn() {
        return getSharedPreferences().getBoolean(Constants.IS_LOGGED_IN, false);
    }

    public void logout() {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.clear();
        editor.apply();
    }


    public void saveUserData(User userInfo) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        Gson gson = new Gson();
        String json = gson.toJson(userInfo);
        editor.putString(Constants.USER_DATA, json);
        editor.apply();
    }


    public User userData() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(Constants.USER_DATA, "");
        return gson.fromJson(json, User.class);
    }


}
