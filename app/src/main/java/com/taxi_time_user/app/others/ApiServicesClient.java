package com.taxi_time_user.app.others;

import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Balance;
import com.taxi_time_user.app.models.Car;
import com.taxi_time_user.app.models.Country;
import com.taxi_time_user.app.models.Notification;
import com.taxi_time_user.app.models.Order;
import com.taxi_time_user.app.models.User;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiServicesClient {
    @GET(URLs.GENERAL + URLs.COUNTRIES + "{lang}")
    Call<ApiResponse<List<Country>>> fetchCountries(@Path("lang") String lang);

    @POST(URLs.USER + URLs.SIGN_UP_IN + "{lang}")
    Call<ApiResponse<String>> signUpIn(@Path("lang") String lang, @Body Map<String, String> body);

    @GET(URLs.CAR + URLs.CARS_TYPES + "{lang}")
    Call<ApiResponse<List<Car>>> fetchCarsTypes(@Path("lang") String lang, @Header("Authorization") String token);

    @GET(URLs.USER + URLs.PROFILE + "{lang}")
    Call<ApiResponse<User>> profile(@Path("lang") String lang, @Header("Authorization") String token);

    @GET(URLs.GENERAL + "1/" + "{lang}")
    Call<ApiResponse<List<TermsContactUs>>> term(@Path("lang") String lang);

    @GET(URLs.GENERAL + "2/" + "{lang}")
    Call<ApiResponse<TermsContactUs>> contactUsInfo(@Path("lang") String lang);

    @GET(URLs.GENERAL + URLs.NOTIFICATIONS + "{lang}")
    Call<ApiResponse<List<Notification>>> notifications(@Path("lang") String lang, @Header("Authorization") String token);

    @GET(URLs.USER + URLs.HISTORY + "{lang}")
    Call<ApiResponse<List<Order>>> history(@Path("lang") String lang, @Header("Authorization") String token);

    @GET(URLs.USER + URLs.BALANCE + "{lang}")
    Call<ApiResponse<Balance>> blance(@Path("lang") String lang, @Header("Authorization") String token);

    @POST(URLs.GENERAL + URLs.CONTACT_US + "{lang}")
    Call<ApiResponse<Void>> contactUs(@Path("lang") String lang, @Header("Authorization") String token, @Body Map<String, String> body);

    @POST(URLs.USER + URLs.UPDATE_PROFILE + "{lang}")
    Call<ApiResponse<Void>> updateProfile(@Path("lang") String lang, @Header("Authorization") String token, @Body Map<String, String> body);

    @GET(URLs.CAR + URLs.REQUEST_ORDER + "{lang}")
    Call<ApiResponse<String>> requestOrder(@Path("lang") String lang, @Header("Authorization") String token, @Body Map<String, String> body);
}
