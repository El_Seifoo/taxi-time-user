package com.taxi_time_user.app.wallet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityWalletBinding;
import com.taxi_time_user.app.models.Balance;

public class WalletActivity extends AppCompatActivity implements WalletPresenter {
    private ActivityWalletBinding binding;
    private WalletViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_wallet);
        viewModel = new ViewModelProvider(this).get(WalletViewModel.class);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);
        binding.setBalance(new Balance(0.0, ""));

        viewModel.requestBalance().observe(this, new Observer<Balance>() {
            @Override
            public void onChanged(Balance balance) {
                binding.setBalance(balance);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");
        actionBar.setElevation(0);
    }

    @Override
    public void onRetryClicked() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestBalance();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
