package com.taxi_time_user.app.wallet;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.Balance;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletViewModel extends AndroidViewModel {
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<String> errorMessage;/* error message of terms request */
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */


    public WalletViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
    }

    /*
        req. fetching app Balance
     */
    private MutableLiveData<Balance> balanceMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<Balance> requestBalance() {
        if (balanceMutableLiveData.getValue() == null) {
            loadBalance();
        }

        return balanceMutableLiveData;
    }

    private void loadBalance() {
        setProgress(View.VISIBLE);

        Call<ApiResponse<Balance>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .blance(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language)),
                        MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));

        call.enqueue(new Callback<ApiResponse<Balance>>() {
            @Override
            public void onResponse(Call<ApiResponse<Balance>> call, Response<ApiResponse<Balance>> response) {
                setProgress(View.GONE);

                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<Balance> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        Balance balance = apiResponse.getData();
                        if (balance == null) {
                            showResponseMessage(apiResponse.getMessage());
                        } else {
                            balanceMutableLiveData.setValue(balance);
                        }
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Balance>> call, Throwable t) {
                setProgress(View.GONE);
                onFailureHandler(t);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        balanceMutableLiveData.setValue(null);// when request fail make it equals null to be able to request balance again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        balanceMutableLiveData.setValue(null);// when request fail make it equals null to be able to request balance again
        showResponseMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }

    // Setters & Getters --- start --- \\
    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setters & Getters --- end --- \\
}
