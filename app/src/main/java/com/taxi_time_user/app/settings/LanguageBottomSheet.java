package com.taxi_time_user.app.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.LanguageBottomSheetLayoutBinding;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;

public class LanguageBottomSheet extends BottomSheetDialogFragment implements LanguagePresenter {
    private LanguageBottomSheetLayoutBinding binding;
    private ParentCommunicator listener;

    public static LanguageBottomSheet newInstance() {
        return new LanguageBottomSheet();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.language_bottom_sheet_layout, container, false);
        binding.setPresenter(this);

        listener = (ParentCommunicator) getContext();

        return binding.getRoot();
    }

    @Override
    public void onButtonClicked(int index) {
        String appLang = MySingleton.getInstance(getContext()).getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language));
        switch (index) {
            case 0:// changing app language to arabic
                if (!appLang.equals(getString(R.string.arabic_key))) {// if app lang is not arabic then change it
                    MySingleton.getInstance(getContext()).saveStringToSharedPref(Constants.APP_LANGUAGE, getString(R.string.arabic_key));
                    // restart app using onLanguage changed fn of parentCommunicator
                    listener.onLanguageChanged();
                } else // do nothing
                    dismiss();
                break;
            case 1:// changing app language to english
                if (!appLang.equals(getString(R.string.english_key))) {// if app lang is not english then change it
                    MySingleton.getInstance(getContext()).saveStringToSharedPref(Constants.APP_LANGUAGE, getString(R.string.english_key));
                    // restart app using onLanguage changed fn of parentCommunicator
                    listener.onLanguageChanged();
                } else // do nothing
                    dismiss();


        }
    }

    protected interface ParentCommunicator {
        void onLanguageChanged();
    }
}
