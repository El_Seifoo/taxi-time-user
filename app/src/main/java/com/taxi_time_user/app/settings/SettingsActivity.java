package com.taxi_time_user.app.settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivitySettingsBinding;
import com.taxi_time_user.app.models.User;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.taxi_time_user.app.sign.SignActivity;

public class SettingsActivity extends AppCompatActivity implements SettingsViewModel.ViewListener, SettingsPresenter, LanguageBottomSheet.ParentCommunicator {
    private ActivitySettingsBinding binding;
    private SettingsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);
        viewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        viewModel.requestProfile().observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                binding.setUser(user);
            }
        });

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getString(R.string.settings));
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateDestination(Intent intent) {
        startActivity(intent);
    }

    @Override
    public User getUser() {
        return binding.getUser();
    }

    @Override
    public void showLanguageBottomSheet() {
        LanguageBottomSheet languageBottomSheet = LanguageBottomSheet.newInstance();
        languageBottomSheet.show(getSupportFragmentManager(), "lang");
    }

    @Override
    public void onClick(int index) {
        viewModel.onItemClicked(index);
    }

    @Override
    public void onLogoutClicked() {
        MySingleton mySingleton = MySingleton.getInstance(getApplicationContext());

        String lang = mySingleton.getStringFromSharedPref(Constants.APP_LANGUAGE, getString(R.string.default_language));
        mySingleton.logout();
        mySingleton.saveStringToSharedPref(Constants.APP_LANGUAGE, lang);

        finish();
        Intent intent = new Intent(this, SignActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    /*
        request profile data again if request is failed at first time
     */
    @Override
    public void onRetry() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestProfile();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLanguageChanged() {
        startActivity(new Intent(this, SignActivity.class));
        finish();
    }
}
