package com.taxi_time_user.app.settings;

public interface SettingsPresenter {
    void onClick(int index);

    void onLogoutClicked();

    void onRetry();
}
