package com.taxi_time_user.app.settings;

public interface LanguagePresenter {
    void onButtonClicked(int index);
}
