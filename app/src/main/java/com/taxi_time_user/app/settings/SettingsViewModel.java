package com.taxi_time_user.app.settings;

import android.app.Application;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.contact_us.ContactUsActivity;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.models.User;
import com.taxi_time_user.app.my_account.MyAccountActivity;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.taxi_time_user.app.terms.TermsActivity;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsViewModel extends AndroidViewModel {
    private ViewListener viewListener;/* communicator betn. view model and its view */
    private ObservableField<Boolean> buttonsClickable;/* flag to disable or enable buttons */
    protected ObservableField<Integer> progress;/* flag to know the status of progress dialog */
    private ObservableField<String> errorMessage;/* response error message displayed on error view*/
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */
    private ObservableField<Boolean> notificationOnOff;/* flag to know the status of notification */

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        buttonsClickable = new ObservableField<>(true);
        progress = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        errorView = new ObservableField<>(View.GONE);
        notificationOnOff = new ObservableField<>(MySingleton.getInstance(application).getBooleanFromSharedPref(Constants.NOTIFICATION_ON_OFF, true));
    }


    /*
        call fn to fetch user data
     */
    private MutableLiveData<User> userMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<User> requestProfile() {
        if (userMutableLiveData.getValue() == null) {
            loadUser();
        }

        return userMutableLiveData;
    }

    private void loadUser() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        Call<ApiResponse<User>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .profile(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language))
                        , MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.USER_TOKEN, ""));

        call.enqueue(new Callback<ApiResponse<User>>() {
            @Override
            public void onResponse(Call<ApiResponse<User>> call, Response<ApiResponse<User>> response) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<User> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        User user = apiResponse.getData();
                        MySingleton.getInstance(getApplication()).saveUserData(user);
                        userMutableLiveData.setValue(user);
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }

                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<User>> call, Throwable t) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                onFailureHandler(t);
            }
        });

    }

    /*
        show error view and show the error message
     */
    private void showResponseMessage(String message) {
        userMutableLiveData.setValue(null);// when request fail make it equals null to be able to request profile again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t) {
        userMutableLiveData.setValue(null);// when request fail make it equals null to be able to request profile again
        showResponseMessage(t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data));
    }


    /*
        handle the action of each action arrow in settings activity
        @param index .. flag to know which action arrow is clicked
                |-> 0 => view user profile
                |-> 2 => security
                |-> 3 => language
                |-> 4 => clear cache
                |-> 5 => terms & privacy policy
                |-> 6 => contact us
     */
    public void onItemClicked(int index) {
        switch (index) {
            case 0:
                Intent intent = new Intent(getApplication(), MyAccountActivity.class);
                intent.putExtra("User", viewListener.getUser());
                viewListener.navigateDestination(intent);
                break;
            case 3:
                viewListener.showLanguageBottomSheet();
                break;
            case 4:
                clearCache();
                break;
            case 5:
                viewListener.navigateDestination(new Intent(getApplication(), TermsActivity.class));
                break;
            case 6:
                viewListener.navigateDestination(new Intent(getApplication(), ContactUsActivity.class));


        }
    }

    private void clearCache() {
        try {
            File dir = getApplication().getCacheDir();
            boolean x = deleteDir(dir);
            if (x)
                viewListener.showToastMessage(getApplication().getString(R.string.cleared));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    // Setters & Getters  ---------> start
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Boolean> getNotificationOnOff() {
        MySingleton.getInstance(getApplication()).saveBooleanToSharedPref(Constants.NOTIFICATION_ON_OFF, notificationOnOff.get());
        return notificationOnOff;
    }

    public void setNotificationOnOff(boolean onOff) {
        this.notificationOnOff.set(onOff);
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage.set(errorMessage);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    public void setErrorView(int errorView) {
        this.errorView.set(errorView);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean clickable) {
        this.buttonsClickable.set(clickable);
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }
    // Setters & Getters  ---------> end

    protected interface ViewListener {
        void showToastMessage(String message);

        void navigateDestination(Intent intent);

        User getUser();

        void showLanguageBottomSheet();
    }
}
