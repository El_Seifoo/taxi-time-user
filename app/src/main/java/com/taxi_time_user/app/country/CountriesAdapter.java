package com.taxi_time_user.app.country;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.CountriesListItemBinding;
import com.taxi_time_user.app.models.Country;

import java.util.List;

public class CountriesAdapter extends RecyclerView.Adapter<CountriesAdapter.Holder> {
    private List<Country> countries;
    private final onCountriesItemClicked listener;

    public CountriesAdapter(onCountriesItemClicked listener) {
        this.listener = listener;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
        notifyDataSetChanged();
    }

    protected interface onCountriesItemClicked {
        void onItemClickListener(String countryName, String countryId);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new Holder((CountriesListItemBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.countries_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.binding.setCountry(countries.get(position));
    }

    @Override
    public int getItemCount() {
        return countries != null ? countries.size() : 0;
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private CountriesListItemBinding binding;

        public Holder(@NonNull CountriesListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Country country = binding.getCountry();
            listener.onItemClickListener(country.getName(), country.getId());
        }
    }

}

