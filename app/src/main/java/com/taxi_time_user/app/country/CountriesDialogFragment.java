package com.taxi_time_user.app.country;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.CountriesDialogFragmentLayoutBinding;
import com.taxi_time_user.app.models.Country;

import java.util.List;

public class CountriesDialogFragment extends DialogFragment implements CountriesPresenter, CountriesAdapter.onCountriesItemClicked {
    private CountriesDialogFragmentLayoutBinding binding;
    private CountriesViewModel viewModel;
    private RecyclerView countriesList;
    private CountriesAdapter adapter;
    private ParentListener parentListener/* communicator between dialog and its parent activity */;

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.8), (int) (getResources().getDisplayMetrics().heightPixels * 0.7));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.countries_dialog_fragment_layout, container, false);
        viewModel = new ViewModelProvider(this).get(CountriesViewModel.class);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);

        parentListener = (ParentListener) getActivity();

        countriesList = binding.countriesList;
        countriesList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        adapter = new CountriesAdapter(this);
        countriesList.setAdapter(adapter);


        viewModel.requestCountries().observe(getViewLifecycleOwner(), new Observer<List<Country>>() {
            @Override
            public void onChanged(List<Country> countries) {
                adapter.setCountries(countries);
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onRetryClicked() {
        viewModel.setErrorView(View.GONE);
        viewModel.requestCountries();
    }

    @Override
    public void onItemClickListener(String countryName, String countryId) {
        parentListener.setCountryId(countryName, countryId);
        dismiss();
    }

    public interface ParentListener {
        void setCountryId(String countryName, String countryId);
    }
}
