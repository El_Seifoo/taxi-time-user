package com.taxi_time_user.app.contact_us;

import android.app.Application;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.models.ApiResponse;
import com.taxi_time_user.app.others.ApiServicesClient;
import com.taxi_time_user.app.others.Constants;
import com.taxi_time_user.app.others.MySingleton;
import com.taxi_time_user.app.others.TermsContactUs;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsViewModel extends AndroidViewModel {
    private ViewListener viewListener;
    private ObservableField<Integer> progress;/* flag to know the status of progress dialog if loading or not */
    private ObservableField<String> errorMessage;/* error message of terms request */
    private ObservableField<Integer> errorView;/* flag to know if error view is visible or not */
    private ObservableField<Boolean> buttonsClickable;/* flag to disable or enable buttons */

    public ContactUsViewModel(@NonNull Application application) {
        super(application);
        progress = new ObservableField<>(View.GONE);
        errorView = new ObservableField<>(View.GONE);
        errorMessage = new ObservableField<>("");
        buttonsClickable = new ObservableField<>(true);
    }

    /*
        req. fetching app terms & conditions
     */
    private MutableLiveData<TermsContactUs> contactsMutableLiveData = new MutableLiveData<>();

    protected MutableLiveData<TermsContactUs> requestContacts() {
        if (contactsMutableLiveData.getValue() == null) {
            loadTerms();
        }

        return contactsMutableLiveData;
    }

    private void loadTerms() {
        setProgress(View.VISIBLE);
        setButtonsClickable(false);
        Call<ApiResponse<TermsContactUs>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .contactUsInfo(MySingleton.getInstance(getApplication()).getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication().getString(R.string.default_language)));

        call.enqueue(new Callback<ApiResponse<TermsContactUs>>() {
            @Override
            public void onResponse(Call<ApiResponse<TermsContactUs>> call, Response<ApiResponse<TermsContactUs>> response) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<TermsContactUs> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        TermsContactUs contacts = apiResponse.getData();
                        if (contacts == null) {
                            showResponseMessage(apiResponse.getMessage());
                        } else {
                            contactsMutableLiveData.setValue(contacts);
                        }
                    } else {// status of response body is false .. show message of body
                        showResponseMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        showResponseMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        showResponseMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<TermsContactUs>> call, Throwable t) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                onFailureHandler(t, 0);
            }
        });
    }

    /*
        show error view and show the error message
     */
    public void showResponseMessage(String message) {
        contactsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request terms again
        setErrorView(View.VISIBLE);
        setErrorMessage(message);
    }

    /*
        decide what the error is and show to user
     */
    public void onFailureHandler(Throwable t, int index) {
        if (index == 0) {
            contactsMutableLiveData.setValue(null);// when request fail make it equals null to be able to request terms again
            showResponseMessage(getExceptionError(t));
        } else {
            viewListener.showToastMessage(getExceptionError(t));
        }
    }

    private String getExceptionError(Throwable t) {
        return t instanceof IOException ?
                getApplication().getString(R.string.no_internet_connection) :
                getApplication().getString(R.string.error_fetching_data);
    }

    public void onButtonClicked(int index) {
        switch (index) {
            case 0:
                setErrorView(View.GONE);
                requestContacts();
                break;
            case 1:
                sendMail();
        }
    }

    protected void sendMail() {
        String subject = viewListener.getSubject();
        if (subject.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.subject_can_not_be_blank));
            return;
        }
        String message = viewListener.getMessage();
        if (message.isEmpty()) {
            viewListener.showToastMessage(getApplication().getString(R.string.message_can_not_be_blank));
            return;
        }

        setProgress(View.VISIBLE);
        setButtonsClickable(false);

        Map<String, String> body = new HashMap<>();
        body.put("subject", subject);
        body.put("message", message);

        Call<ApiResponse<Void>> call = MySingleton.getInstance(getApplication())
                .createService(ApiServicesClient.class, 0)
                .contactUs(MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.APP_LANGUAGE, getApplication()
                                        .getString(R.string.default_language)),
                        MySingleton.getInstance(getApplication())
                                .getStringFromSharedPref(Constants.USER_TOKEN, ""),
                        body);

        call.enqueue(new Callback<ApiResponse<Void>>() {
            @Override
            public void onResponse(Call<ApiResponse<Void>> call, Response<ApiResponse<Void>> response) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                int respCode = response.code();
                if (respCode == 200) {// request succeeded
                    ApiResponse<Void> apiResponse = response.body();
                    if (apiResponse.getStatus().equals(getApplication().getString(R.string.api_response_status_true))) {// status of response body is true
                        viewListener.showToastMessage(getApplication().getString(R.string.message_sent_successfully));
                        viewListener.clearSubjectMessage();
                    } else {
                        viewListener.showToastMessage(apiResponse.getMessage());
                    }
                } else if (respCode == 500) {// internal server error message
                    viewListener.showToastMessage(getApplication().getString(R.string.error_sending_data));
                } else {// req. failed for any reason .. show message of error response
                    try {
                        JSONObject jsonObject = new JSONObject(response.errorBody().string());
                        viewListener.showToastMessage(jsonObject.getString("message"));
                    } catch (Exception e) {
                        viewListener.showToastMessage(getApplication().getString(R.string.error_fetching_data));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Void>> call, Throwable t) {
                setProgress(View.GONE);
                setButtonsClickable(true);
                onFailureHandler(t, 1);
            }
        });


    }

    // Setters & Getters --- start --- \\
    public void setViewListener(ViewListener viewListener) {
        this.viewListener = viewListener;
    }

    public ObservableField<Integer> getProgress() {
        return progress;
    }

    public ObservableField<String> getErrorMessage() {
        return errorMessage;
    }

    protected void setErrorMessage(String message) {
        errorMessage.set(message);
    }

    public ObservableField<Integer> getErrorView() {
        return errorView;
    }

    protected void setErrorView(int errorViewStatus) {
        this.errorView.set(errorViewStatus);
    }

    public void setProgress(int progress) {
        this.progress.set(progress);
    }

    public ObservableField<Boolean> getButtonsClickable() {
        return buttonsClickable;
    }

    public void setButtonsClickable(boolean buttonsClickable) {
        this.buttonsClickable.set(buttonsClickable);
    }// Setters & Getters --- end --- \\

    protected interface ViewListener {
        void showToastMessage(String message);

        String getSubject();

        String getMessage();

        void clearSubjectMessage();
    }
}
