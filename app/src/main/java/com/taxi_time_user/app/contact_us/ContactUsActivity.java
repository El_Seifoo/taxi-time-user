package com.taxi_time_user.app.contact_us;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.Toast;

import com.taxi_time_user.app.R;
import com.taxi_time_user.app.databinding.ActivityContactUsBinding;
import com.taxi_time_user.app.others.TermsContactUs;

public class ContactUsActivity extends AppCompatActivity implements ContactUsPresenter, ContactUsViewModel.ViewListener {
    private ActivityContactUsBinding binding;
    private ContactUsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        viewModel = new ViewModelProvider(this).get(ContactUsViewModel.class);
        viewModel.setViewListener(this);
        binding.setViewModel(viewModel);
        binding.setPresenter(this);
        binding.setContacts(new TermsContactUs("", "", ""));

        viewModel.requestContacts().observe(this, new Observer<TermsContactUs>() {
            @Override
            public void onChanged(TermsContactUs contacts) {
                binding.setContacts(contacts);
            }
        });
    }

    @Override
    public void onButtonClicked(int index) {
        viewModel.onButtonClicked(index);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public String getSubject() {
        return binding.subjectEditText.getText().toString();
    }

    @Override
    public String getMessage() {
        return binding.messageEditText.getText().toString();
    }

    @Override
    public void clearSubjectMessage() {
        binding.subjectEditText.setText("");
        binding.messageEditText.setText("");
    }
}
