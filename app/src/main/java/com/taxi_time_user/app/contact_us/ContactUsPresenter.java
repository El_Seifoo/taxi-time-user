package com.taxi_time_user.app.contact_us;

public interface ContactUsPresenter {
    void onButtonClicked(int index);
}
