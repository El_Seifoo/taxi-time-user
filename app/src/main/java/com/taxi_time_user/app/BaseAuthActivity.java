package com.taxi_time_user.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.taxi_time_user.app.others.Constants;

import java.util.ArrayList;

import timber.log.Timber;

public class BaseAuthActivity extends BaseActivity {

    protected ArrayList<String> arrayList;
    private CallbackManager callbackManager;
    private FirebaseAuth authProvider;
    private GoogleSignInClient googleSignInClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authProvider = FirebaseAuth.getInstance();
        facebookSetup();
        setupGoogleSignInAccount();

    }

    public void setupGoogleSignInAccount() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(this, gso);

        googleSignInClient.signOut();
    }

    private void facebookSetup() {
        arrayList = new ArrayList<>();
        arrayList.add("public_profile");
        arrayList.add("email");

        AppEventsLogger.activateApp(getApplication());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.d(String.format("onSuccess: getAccessToken%s", loginResult.getAccessToken()));
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Timber.e("onCancel: ");
            }

            @Override
            public void onError(FacebookException exception) {
                Timber.e(exception, "onError: ");
                Toast.makeText(BaseAuthActivity.this, "" + exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void googleClicked(View view) {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, Constants.google_code);
    }

    public void facebookClicked(View view) {
        LoginManager.getInstance().logInWithReadPermissions(this, arrayList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.google_code) {
            onGetLoginByGoogleResultHandler(data);
        } else
            callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Timber.d("handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        authProvider.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in u ser's information
                        Timber.d("signInWithCredential:success");
                        FirebaseUser user = authProvider.getCurrentUser();
                        if (user != null)
                            updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Timber.e(task.getException());
                        Toast.makeText(BaseAuthActivity.this, "" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        //  updateUI(null);
                    }

                    // ...
                });
    }

    private void updateUI(FirebaseUser user) {
        Timber.d("updateUI: user : " + user.toString());
        registerNewUser(user);
    }

    private void registerNewUser(FirebaseUser user) {
        // TODO: 11/9/2020  
//        authViewModel.requestRegisterLoginFaceBook(
//                modelGoogleAuthRequestBuilder
//                        .setEmail(user.getEmail())
//                        .setName(user.getDisplayName())
//                        .setPicture(String.valueOf(user.getPhotoUrl()))
//                        .setUser_mobile(user.getPhoneNumber()).build());
    }

    private void onGetLoginByGoogleResultHandler(Intent data) {
        try {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            GoogleSignInAccount googleAccount = task.getResult(ApiException.class);
            onLoggedIn(googleAccount);
        } catch (ApiException e) {
            Timber.w("signInResult:failed code=%s", e.getStatusCode());
        }
    }

    private void onLoggedIn(GoogleSignInAccount googleSignInAccount) {
// TODO: 11/9/2020
//        modelGoogleAuthRequestBuilder.setEmail(googleSignInAccount.getEmail())
//                .setName(googleSignInAccount.getDisplayName())
////                .setUser_mobile(googleSignInAccount.get())
//                .setPicture(googleSignInAccount.getPhotoUrl() != null ? googleSignInAccount.getPhotoUrl().toString() : null);
//
//        authViewModel.requestLoginGoogle(modelGoogleAuthRequestBuilder.build());
    }


}
